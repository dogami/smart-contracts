alias ligo="docker run --rm -v "$PWD":"$PWD" -w "$PWD" ligolang/ligo:0.46.1"
ligo compile contract nft/nft.mligo --protocol jakarta > michelson/nft.tz
ligo compile contract multisig/multisig.mligo --protocol jakarta > michelson/multisig.tz
ligo compile contract mint/mint.mligo --protocol jakarta > michelson/mint.tz
ligo compile contract reveal/reveal.mligo --protocol jakarta > michelson/reveal.tz
ligo compile contract factory/factory.mligo --protocol jakarta > michelson/factory.tz
ligo compile contract factory/launchers/mint_launcher/mint_launcher.mligo --protocol jakarta > michelson/mint_launcher.tz
ligo compile contract factory/launchers/reveal_launcher/reveal_launcher.mligo --protocol jakarta > michelson/reveal_launcher.tz
ligo compile contract factory/launchers/nft_launcher/nft_launcher.mligo --protocol jakarta > michelson/nft_launcher.tz
ligo compile contract factory/controllers/mint_controller/mint_controller.mligo --protocol jakarta > michelson/mint_controller.tz
ligo compile contract factory/controllers/reveal_controller/reveal_controller.mligo --protocol jakarta > michelson/reveal_controller.tz
ligo compile contract factory/controllers/nft_controller/nft_controller.mligo --protocol jakarta > michelson/nft_controller.tz
ligo compile contract marketplace/marketplace.mligo --protocol jakarta > michelson/marketplace.tz
