import sys
from pytezos import pytezos, ContractInterface
from pytezos.contract.result import OperationResult
from dataclasses import dataclass
import logging
logging.basicConfig(level=logging.INFO)


send_conf = dict(min_confirmations=1)

network = sys.argv[1]

if network == "sandbox":
    ALICE_KEY = "edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q"
    ALICE_PK = "tz1Yigc57GHQixFwDEVzj5N1znSCU3aq15td"
    SHELL = "http://localhost:8732"

elif network == "testnet":
    SHELL = "https://rpc.ghostnet.teztnets.xyz/"
    ALICE_KEY = "edsk3Qbsce5hgRcG6KYDQGyrZdyv4FQSQBenSt4RRsGKx9KnQfoY37"
    ALICE_PK = "tz1X92FqzD65C8ouXL12VauX1RUgZaURJQ2Y"

elif network == "mainnet":
    SHELL = "https://rpc.tzstats.com"
    ALICE_PK = input("Please provide a tz... address for the protocol admin: ")
    ALICE_KEY = input("Please provide the admin's secret key: ")

else:
    print("No valid network was supplied.")
    exit()

alice_using_params = dict(shell=SHELL, key=ALICE_KEY)

alice_pytezos = pytezos.using(**alice_using_params)


@ dataclass
class MultisigStorage:
    authorized_contracts: str = ALICE_PK
    admins: str = ALICE_PK


@dataclass
class FactoryStorage:
    multisig: str = ALICE_PK
    nft_address: str = ALICE_PK
    doga_address: str = ALICE_PK
    reserve_address: str = ALICE_PK
    oracle_address: str = ALICE_PK


def deploy_multisig():
    with open("michelson/multisig.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()

    multisig = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)

    init_storage = MultisigStorage()

    storage = {
        "admins": {init_storage.admins},
        "n_calls": {},
        "threshold": 1,
        "duration": 3600,
        "authorized_contracts": {init_storage.authorized_contracts},
    }
    opg = multisig.originate(initial_storage=storage).send(**send_conf)
    multisig_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    multisig = pytezos.using(
        **alice_using_params).contract(multisig_addr)

    return multisig


def deploy_mint_launcher(factory_address: str, controller_address: str):
    with open("michelson/mint_launcher.tz") as mich_file:
        michelson = mich_file.read()
    mint_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = mint_launcher.originate(
        initial_storage=storage).send(**send_conf)
    mint_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    mint_launcher = pytezos.using(
        **alice_using_params).contract(mint_launcher_address)

    return mint_launcher


def deploy_reveal_launcher(factory_address: str, controller_address: str):
    with open("michelson/reveal_launcher.tz") as mich_file:
        michelson = mich_file.read()
    reveal_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = reveal_launcher.originate(
        initial_storage=storage).send(**send_conf)
    reveal_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    reveal_launcher = pytezos.using(
        **alice_using_params).contract(reveal_launcher_address)

    return reveal_launcher


def deploy_nft_launcher(factory_address: str, controller_address: str):
    with open("michelson/nft_launcher.tz") as mich_file:
        michelson = mich_file.read()
    nft_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = nft_launcher.originate(
        initial_storage=storage).send(**send_conf)
    nft_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    nft_launcher = pytezos.using(
        **alice_using_params).contract(nft_launcher_address)

    return nft_launcher


def deploy_mint_controller(multisig: str, factory: str):
    with open("michelson/mint_controller.tz") as mich_file:
        michelson = mich_file.read()
    mint_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = mint_controller.originate(
        initial_storage=storage).send(**send_conf)
    mint_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    mint_controller = pytezos.using(
        **alice_using_params).contract(mint_controller_address)

    return mint_controller


def deploy_reveal_controller(multisig: str, factory: str):
    with open("michelson/reveal_controller.tz") as mich_file:
        michelson = mich_file.read()
    reveal_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = reveal_controller.originate(
        initial_storage=storage).send(**send_conf)
    reveal_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    reveal_controller = pytezos.using(
        **alice_using_params).contract(reveal_controller_address)

    return reveal_controller


def deploy_nft_controller(multisig: str, factory: str):
    with open("michelson/nft_controller.tz") as mich_file:
        michelson = mich_file.read()
    nft_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = nft_controller.originate(
        initial_storage=storage).send(**send_conf)
    nft_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    nft_controller = pytezos.using(
        **alice_using_params).contract(nft_controller_address)

    return nft_controller


def deploy_factory(multisig=ALICE_PK):
    init_storage = FactoryStorage(multisig=multisig)

    with open("michelson/factory.tz", "r", encoding="UTF-8") as file:
        michelson = file.read()

    storage = {
        "last_deployed_contracts": {"minter": ALICE_PK, "revealer": ALICE_PK, "nft": ALICE_PK},
        "contract_sets": {},
        "multisig": init_storage.multisig,
        "doga_address": init_storage.doga_address,
        "default_reserve": init_storage.reserve_address,
        "default_oracle": init_storage.oracle_address,
        "mint_launcher": ALICE_PK,
        "reveal_launcher": ALICE_PK,
        "nft_launcher": ALICE_PK,
        "mint_controller": ALICE_PK,
        "reveal_controller": ALICE_PK,
        "nft_controller": ALICE_PK,
    }

    factory = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    opg = factory.originate(initial_storage=storage).send(**send_conf)
    factory_addr = OperationResult.from_operation_group(
        opg.opg_result)[0].originated_contracts[0]
    factory = alice_pytezos.using(
        **alice_using_params).contract(factory_addr)

    return factory


def deploy_factory_app():
    multisig = deploy_multisig()
    factory = deploy_factory(multisig.address)
    nft_controller = deploy_nft_controller(
        multisig=multisig.address, factory=factory.address)
    mint_controller = deploy_mint_controller(
        multisig=multisig.address, factory=factory.address)
    reveal_controller = deploy_reveal_controller(
        multisig=multisig.address, factory=factory.address)
    nft_launcher = deploy_nft_launcher(
        factory_address=factory.address, controller_address=nft_controller.address)
    mint_launcher = deploy_mint_launcher(
        factory_address=factory.address, controller_address=mint_controller.address)
    reveal_launcher = deploy_reveal_launcher(
        factory_address=factory.address, controller_address=reveal_controller.address)
    multisig.addAuthorizedContract(factory.address).send(**send_conf)
    multisig.addAuthorizedContract(
        nft_controller.address).send(**send_conf)
    multisig.addAuthorizedContract(
        mint_controller.address).send(**send_conf)
    multisig.addAuthorizedContract(
        reveal_controller.address).send(**send_conf)
    factory.updateLaunchers({"mint_launcher": mint_launcher.address,
                            "reveal_launcher": reveal_launcher.address, "nft_launcher": nft_launcher.address}).send(**send_conf)
    factory.updateControllers({"mint_controller": mint_controller.address,
                              "reveal_controller": reveal_controller.address, "nft_controller": nft_controller.address}).send(**send_conf)

    return factory, multisig, mint_controller, reveal_controller, nft_controller


factory, multisig, mint_controller, reveal_controller, nft_controller = deploy_factory_app()

print(f"factory address : {factory.address}")
print(f"multisig address : {multisig.address}")
print(f"mint_controller address : {mint_controller.address}")
print(f"reveal_controller address : {reveal_controller.address}")
print(f"nft_controller address : {nft_controller.address}")
