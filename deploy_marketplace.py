import sys
from pytezos import pytezos, ContractInterface
from pytezos.contract.result import OperationResult
from dataclasses import dataclass
import logging
from tests.data import normalizer_data
logging.basicConfig(level=logging.INFO)


send_conf = dict(min_confirmations=3)

network = sys.argv[1]


def deploy_normalizer():
    with open("michelson/normalizer.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()
    normalizer = ContractInterface.from_michelson(michelson).using(
        **alice_using_params
    )
    storage = normalizer_data
    opg = normalizer.originate(initial_storage=storage).send(**send_conf)
    normalizer_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    normalizer = pytezos.using(
        **alice_using_params).contract(normalizer_addr)

    return normalizer


def deploy_doga():
    with open("michelson/doga.tz", "r", encoding="UTF8") as file:
        michelson = file.read()
    storage = {
        "paused": False,
        "burn_paused": False,
        "ledger": {ALICE_PK: 1_000_000_000},
        "allowances": {},
        "total_supply": 1_000_000_000,
        "metadata": {},
        "token_metadata": {},
        "multisig": ALICE_PK,
    }
    doga = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    opg = doga.originate(initial_storage=storage).send(**send_conf)
    doga_addr = OperationResult.from_operation_group(
        opg.opg_result)[0].originated_contracts[0]
    doga = alice_pytezos.using(
        **alice_using_params).contract(doga_addr)
    return doga


if network == "sandbox":
    ALICE_KEY = "edsk3EQB2zJvvGrMKzkUxhgERsy6qdDDw19TQyFWkYNUmGSxXiYm7Q"
    ALICE_PK = "tz1Yigc57GHQixFwDEVzj5N1znSCU3aq15td"
    SHELL = "http://localhost:8732"

elif network == "testnet":
    SHELL = "https://rpc.ghostnet.teztnets.xyz/"
    ALICE_KEY = "edsk3Qbsce5hgRcG6KYDQGyrZdyv4FQSQBenSt4RRsGKx9KnQfoY37"
    ALICE_PK = "tz1X92FqzD65C8ouXL12VauX1RUgZaURJQ2Y"

elif network == "mainnet":
    SHELL = "https://rpc.tzstats.com"
    ALICE_PK = input("Please provide a tz... address for the protocol admin: ")
    ALICE_KEY = input("Please provide the admin's secret key: ")

else:
    print("No valid network was supplied.")
    exit()

alice_using_params = dict(shell=SHELL, key=ALICE_KEY)

alice_pytezos = pytezos.using(**alice_using_params)


@ dataclass
class FA12Storage:
    admin: str = ALICE_PK


def deploy_normalizer():
    with open("michelson/normalizer.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()
    normalizer = ContractInterface.from_michelson(michelson).using(
        **alice_using_params
    )
    storage = normalizer_data
    opg = normalizer.originate(initial_storage=storage).send(**send_conf)
    normalizer_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    normalizer = pytezos.using(
        **alice_using_params).contract(normalizer_addr)

    return normalizer


def deploy_doga():
    with open("michelson/doga.tz", "r", encoding="UTF8") as file:
        michelson = file.read()
    storage = {
        "paused": False,
        "burn_paused": False,
        "ledger": {ALICE_PK: 1_000_000_000},
        "allowances": {},
        "total_supply": 1_000_000_000,
        "metadata": {},
        "token_metadata": {},
        "multisig": ALICE_PK,
    }
    doga = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    opg = doga.originate(initial_storage=storage).send(**send_conf)
    doga_addr = OperationResult.from_operation_group(
        opg.opg_result)[0].originated_contracts[0]
    doga = alice_pytezos.using(
        **alice_using_params).contract(doga_addr)
    return doga


def deploy_fa12(init_storage: FA12Storage):
    with open("michelson/fa12.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()
    fa12 = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "tokens": {},
        "allowances": {},
        "total_supply": 0,
        "metadata": {},
        "token_metadata": {},
        "paused": False,
        "admin": init_storage.admin
    }
    opg = fa12.originate(initial_storage=storage).send(**send_conf)
    fa12_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    fa12 = pytezos.using(**alice_using_params).contract(fa12_addr)

    return fa12


if network == "mainnet":
    USD = "KT1K9gCRgaLRFKTErYt1wVxA3Frb9FjasjTV"
    NORMALIZER = "KT1KBrn1udLLrGNbQ3n1mWgMVXkr26krj6Nj"
    DOGA = "KT1Ha4yFVeyzw6KRAdkzq6TxDHB97KG4pZe8"
elif network == "testnet":
    usd = deploy_fa12(FA12Storage)
    USD = usd.address
    NORMALIZER = "KT1ENe4jbDE1QVG1euryp23GsAeWuEwJutQX"
    doga = deploy_doga()
    DOGA = doga.address
else:
    usd = deploy_fa12(FA12Storage)
    USD = usd.address
    normalizer = deploy_normalizer()
    NORMALIZER = normalizer.address
    doga = deploy_doga()
    DOGA = doga.address


@ dataclass
class MarketplaceStorage:
    multisig: str
    treasury: str = ALICE_PK
    nft_address: str = ALICE_PK
    royalties_address: str = ALICE_PK
    oracle: str = NORMALIZER
    xtz_address: str = ALICE_PK
    usd_address: str = USD
    doga_address: str = DOGA
    oracle_tolerance: int = 900


@ dataclass
class MultisigStorage:
    authorized_contracts: str = ALICE_PK
    admins: str = ALICE_PK


@dataclass
class FactoryStorage:
    multisig: str = ALICE_PK
    doga_address: str = DOGA
    reserve_address: str = ALICE_PK
    oracle_address: str = NORMALIZER


def deploy_multisig():
    with open("michelson/multisig.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()

    multisig = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)

    init_storage = MultisigStorage()

    storage = {
        "admins": {init_storage.admins},
        "n_calls": {},
        "threshold": 1,
        "duration": 3600,
        "authorized_contracts": {init_storage.authorized_contracts},
    }
    opg = multisig.originate(initial_storage=storage).send(**send_conf)
    multisig_addr = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    multisig = pytezos.using(
        **alice_using_params).contract(multisig_addr)

    return multisig


def deploy_mint_launcher(factory_address: str, controller_address: str):
    with open("michelson/mint_launcher.tz") as mich_file:
        michelson = mich_file.read()
    mint_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = mint_launcher.originate(
        initial_storage=storage).send(**send_conf)
    mint_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    mint_launcher = pytezos.using(
        **alice_using_params).contract(mint_launcher_address)

    return mint_launcher


def deploy_reveal_launcher(factory_address: str, controller_address: str):
    with open("michelson/reveal_launcher.tz") as mich_file:
        michelson = mich_file.read()
    reveal_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = reveal_launcher.originate(
        initial_storage=storage).send(**send_conf)
    reveal_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    reveal_launcher = pytezos.using(
        **alice_using_params).contract(reveal_launcher_address)

    return reveal_launcher


def deploy_nft_launcher(factory_address: str, controller_address: str):
    with open("michelson/nft_launcher.tz") as mich_file:
        michelson = mich_file.read()
    nft_launcher = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "controller": controller_address,
        "factory": factory_address,
    }
    opg = nft_launcher.originate(
        initial_storage=storage).send(**send_conf)
    nft_launcher_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    nft_launcher = pytezos.using(
        **alice_using_params).contract(nft_launcher_address)

    return nft_launcher


def deploy_mint_controller(multisig: str, factory: str):
    with open("michelson/mint_controller.tz") as mich_file:
        michelson = mich_file.read()
    mint_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = mint_controller.originate(
        initial_storage=storage).send(**send_conf)
    mint_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    mint_controller = pytezos.using(
        **alice_using_params).contract(mint_controller_address)

    return mint_controller


def deploy_reveal_controller(multisig: str, factory: str):
    with open("michelson/reveal_controller.tz") as mich_file:
        michelson = mich_file.read()
    reveal_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = reveal_controller.originate(
        initial_storage=storage).send(**send_conf)
    reveal_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    reveal_controller = pytezos.using(
        **alice_using_params).contract(reveal_controller_address)

    return reveal_controller


def deploy_nft_controller(multisig: str, factory: str):
    with open("michelson/nft_controller.tz") as mich_file:
        michelson = mich_file.read()
    nft_controller = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    storage = {
        "multisig": multisig,
        "factory": factory,
    }
    opg = nft_controller.originate(
        initial_storage=storage).send(**send_conf)
    nft_controller_address = OperationResult.from_operation_group(opg.opg_result)[
        0].originated_contracts[0]
    nft_controller = pytezos.using(
        **alice_using_params).contract(nft_controller_address)

    return nft_controller


def deploy_factory(multisig=ALICE_PK):
    init_storage = FactoryStorage(multisig=multisig)

    with open("michelson/factory.tz", "r", encoding="UTF-8") as file:
        michelson = file.read()

    storage = {
        "last_deployed_contracts": {"minter": ALICE_PK, "revealer": ALICE_PK, "nft": ALICE_PK},
        "contract_sets": {},
        "multisig": init_storage.multisig,
        "doga_address": init_storage.doga_address,
        "default_reserve": init_storage.reserve_address,
        "default_oracle": init_storage.oracle_address,
        "mint_launcher": ALICE_PK,
        "reveal_launcher": ALICE_PK,
        "nft_launcher": ALICE_PK,
        "mint_controller": ALICE_PK,
        "reveal_controller": ALICE_PK,
        "nft_controller": ALICE_PK,
    }

    factory = ContractInterface.from_michelson(
        michelson).using(**alice_using_params)
    opg = factory.originate(initial_storage=storage).send(**send_conf)
    factory_addr = OperationResult.from_operation_group(
        opg.opg_result)[0].originated_contracts[0]
    factory = alice_pytezos.using(
        **alice_using_params).contract(factory_addr)

    return factory


def deploy_factory_app():
    multisig = deploy_multisig()
    factory = deploy_factory(multisig.address)
    nft_controller = deploy_nft_controller(
        multisig=multisig.address, factory=factory.address)
    mint_controller = deploy_mint_controller(
        multisig=multisig.address, factory=factory.address)
    reveal_controller = deploy_reveal_controller(
        multisig=multisig.address, factory=factory.address)
    nft_launcher = deploy_nft_launcher(
        factory_address=factory.address, controller_address=nft_controller.address)
    mint_launcher = deploy_mint_launcher(
        factory_address=factory.address, controller_address=mint_controller.address)
    reveal_launcher = deploy_reveal_launcher(
        factory_address=factory.address, controller_address=reveal_controller.address)
    multisig.addAuthorizedContract(factory.address).send(**send_conf)
    multisig.addAuthorizedContract(
        nft_controller.address).send(**send_conf)
    multisig.addAuthorizedContract(
        mint_controller.address).send(**send_conf)
    multisig.addAuthorizedContract(
        reveal_controller.address).send(**send_conf)
    factory.updateLaunchers({"mint_launcher": mint_launcher.address,
                            "reveal_launcher": reveal_launcher.address, "nft_launcher": nft_launcher.address}).send(**send_conf)
    factory.updateControllers({"mint_controller": mint_controller.address,
                              "reveal_controller": reveal_controller.address, "nft_controller": nft_controller.address}).send(**send_conf)

    return factory, multisig, mint_controller, reveal_controller, nft_controller


def deploy_marketplace(init_storage: MarketplaceStorage):

    with open("michelson/marketplace.tz", encoding="UTF-8") as mich_file:
        michelson = mich_file.read()
    marketplace = ContractInterface.from_michelson(michelson).using(
        **alice_using_params
    )
    storage = {
        "multisig": init_storage.multisig,
        "treasury": init_storage.treasury,
        "collections": {init_storage.nft_address},
        "royalties_address": init_storage.royalties_address,
        "royalties_rate": 200,
        "next_swap_id": 0,
        "tokens": {},
        "swaps": {},
        "offers": {},
        "counter_offers": {},
        "management_fee_rate": 250,
        "paused": False,
        "allowed_tokens": {
            "XTZ": {
                "token_symbol": "XTZ",
                "fa_address": init_storage.xtz_address,
                "fa_type": "XTZ"
            },
            "USD": {
                "token_symbol": "USD",
                "fa_address": init_storage.usd_address,
                "fa_type": "fa1.2"
            },
            "DOGA": {
                "token_symbol": "DOGA",
                "fa_address": init_storage.doga_address,
                "fa_type": "fa1.2"
            }
        },
        "available_pairs": {
            ("XTZ", "USD"): "XTZ-USD",
        },
        "oracle": init_storage.oracle,
        "oracle_tolerance": init_storage.oracle_tolerance,
    }
    opg = marketplace.originate(initial_storage=storage).send(**send_conf)
    marketplace_address = OperationResult.from_operation_group(opg.opg_result)[
        0
    ].originated_contracts[0]
    marketplace = pytezos.using(
        **alice_using_params).contract(marketplace_address)
    return marketplace


def deploy_marketplace_app():
    factory, multisig, mint_controller, reveal_controller, nft_controller = deploy_factory_app()
    marketplace_init_storage = MarketplaceStorage(
        multisig=multisig.address)
    marketplace = deploy_marketplace(marketplace_init_storage)
    multisig.addAuthorizedContract(marketplace.address).send(**send_conf)

    return marketplace, factory, multisig


marketplace, factory, multisig = deploy_marketplace_app()

print(f"factory address : {factory.address}")
print(f"multisig address : {multisig.address}")
print(f"marketplace address : {marketplace.address}")
