type set_id = Factory.set_id

type storage = 
[@layout:comb]
{
    factory : address;
    multisig : address;
}

type set_mint_params =
[@layout:comb]
{
    set_mint_params : Mint.set_mint_params_param;
    set_id : set_id;
}

type add_to_whitelist_param =
[@layout:comb]
{
    add_to_whitelist_param : Mint.add_to_whitelist_param;
    set_id : set_id;
}

type remove_from_whitelist_param =
[@layout:comb]
{
    remove_from_whitelist_param : Mint.remove_from_whitelist_param;
    set_id : set_id;
}

type set_default_metadata_param =
[@layout:comb]
{
    set_default_metadata_param : Mint.set_default_metadata_param;
    set_id : set_id;
}

type burn_from_crowdsale_param =
[@layout:comb]
{
    burn_from_crowdsale_param : Mint.burn_from_crowdsale_param;
    set_id : set_id;
}

type set_name_list_param =
[@layout:comb]
{
    set_name_list_param : Mint.set_name_list_param;
    set_id : set_id;
}

type set_doga_address_param =
[@layout:comb]
{
    update_method : Factory.update_method;
    doga_address : address;
}

type set_reserve_address_param =
[@layout:comb]
{
    update_method : Factory.update_method;
    reserve_address : address;
}

type set_mint_per_address_param = set_id

type parameter =
| SetMintParams of set_mint_params
| SetMintPerAddress of set_mint_per_address_param
| AddToWhitelist of add_to_whitelist_param
| RemoveFromWhitelist of remove_from_whitelist_param
| SetDefaultMetadata of set_default_metadata_param
| BurnFromCrowdsale of burn_from_crowdsale_param
| SetNameList of set_name_list_param
| SetDogaAddress of set_doga_address_param
| SetReserveAddress of set_reserve_address_param
| UpdateMultisig of address
| UpdateFactory of address

type return = operation list * storage