#import "../../factory_interface.mligo" "Factory"
#import "../../../nft/nft.mligo" "Nft"
#include "nft_controller_interface.mligo"
#include "../common/common_functions.mligo"


let set_metadata (param : set_metadata_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
          match (Tezos.get_entrypoint_opt "%setMetadata" sender_address : set_metadata_param contract option) with
          | None -> (failwith("no setMetadata entrypoint") : operation list)
          | Some set_metadata_entrypoint -> [Tezos.transaction param 0mutez set_metadata_entrypoint] in
        (prepare_multisig "setMetadata" param func store), store
    else
        let {
            set_metadata_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setMetadata" set.nft : Nft.set_metadata_param contract option) with
        | None -> (failwith("no setMetadata for this nft") : return)
        | Some contr -> [Tezos.transaction set_metadata_param 0mutez contr], store

let set_burn_pause (param : set_burn_pause_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setBurnPause" sender_address : set_burn_pause_param contract option) with
            | None -> (failwith("no setBurnPause entrypoint") : operation list)
            | Some set_burn_pause_entrypoint -> [Tezos.transaction param 0mutez set_burn_pause_entrypoint] in
        (prepare_multisig "setBurnPause" param func store), store
    else
        let {
            set_burn_pause_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setBurnPause" set.nft : Nft.set_burn_pause_param contract option) with
        | None -> (failwith("no setBurnPause for this nft") : return)
        | Some contr -> [Tezos.transaction set_burn_pause_param 0mutez contr], store

let set_contract_pause (param : set_contract_pause_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setContractPause" sender_address : set_contract_pause_param contract option) with
            | None -> (failwith("no setContractPause entrypoint") : operation list)
            | Some set_contract_pause_entrypoint -> [Tezos.transaction param 0mutez set_contract_pause_entrypoint] in
        (prepare_multisig "setContractPause" param func store), store
    else
        let {
            set_contract_pause_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setContractPause" set.nft : Nft.set_contract_pause_param contract option) with
        | None -> (failwith("no setContractPause for this nft") : return)
        | Some contr -> [Tezos.transaction set_contract_pause_param 0mutez contr], store

let remove_contract (param : remove_contract_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%removeContract" sender_address : remove_contract_param contract option) with
            | None -> (failwith("no removeContract entrypoint") : operation list)
            | Some remove_contract_entrypoint -> [Tezos.transaction param 0mutez remove_contract_entrypoint] in
        (prepare_multisig "removeContract" param func store), store
    else
        let {
            remove_contract_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%removeContract" set.nft : Nft.remove_contract_param contract option) with
        | None -> (failwith("no removeContract for this nft") : return)
        | Some contr -> [Tezos.transaction remove_contract_param 0mutez contr], store

let add_contract (param : add_contract_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%addContract" sender_address : add_contract_param contract option) with
            | None -> (failwith("no addContract entrypoint") : operation list)
            | Some add_contract_entrypoint -> [Tezos.transaction param 0mutez add_contract_entrypoint] in
        (prepare_multisig "addContract" param func store), store
    else
        let {
            add_contract_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%addContract" set.nft : Nft.add_contract_param contract option) with
        | None -> (failwith("no addContract for this nft") : return)
        | Some contr -> [Tezos.transaction add_contract_param 0mutez contr], store

let update_multisig (param : address) (store : storage) : return =
    if (Tezos.get_sender () <> store.factory) then
        (failwith("only factory can call this entrypoint") : return)
    else
        ([] : operation list), {store with multisig = param}

let update_factory (param : address) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%updateFactory" sender_address : address contract option) with
            | None -> (failwith("no updateFactory entrypoint") : operation list)
            | Some update_factory_entrypoint -> [Tezos.transaction param 0mutez update_factory_entrypoint] in
        (prepare_multisig "updateFactory" param func store), store
    else
        ([] : operation list), {store with factory = param}

let set_factory (nft_address : address) (store : storage) : return =
    if Tezos.get_sender () <> store.factory then
        (failwith("only factory can call this entrypoint") : return)
    else
        let ops = [Tezos.transaction store.factory 0mutez (get_set_factory_entrypoint nft_address)] in
    ops, store

let main (action, store : parameter * storage) : return =
match action with
| SetMetadata p -> set_metadata p store
| SetBurnPause p -> set_burn_pause p store
| SetContractPause p -> set_contract_pause p store
| AddContract p -> add_contract p store
| RemoveContract p -> remove_contract p store
| UpdateMultisig p -> update_multisig p store
| UpdateFactory p -> update_factory p store
| SetFactory p -> set_factory p store
