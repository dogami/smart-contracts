type set_id = Factory.set_id

type set_metadata_param =
[@layout:comb]
{
    set_metadata_param : Nft.set_metadata_param;
    set_id : set_id;
}

type set_burn_pause_param =
[@layout:comb]
{
    set_burn_pause_param : Nft.set_burn_pause_param;
    set_id : set_id;
}

type set_contract_pause_param =
[@layout:comb]
{
    set_contract_pause_param : Nft.set_contract_pause_param;
    set_id : set_id;
}

type remove_contract_param =
[@layout:comb]
{
    remove_contract_param : Nft.remove_contract_param;
    set_id : set_id;
}

type add_contract_param =
[@layout:comb]
{
    add_contract_param : Nft.add_contract_param;
    set_id : set_id;
}

type storage =
[@layout:comb]
{
    factory : address;
    multisig : address;
}

type return = operation list * storage

type parameter =
| SetMetadata of set_metadata_param
| SetBurnPause of set_burn_pause_param
| SetContractPause of set_contract_pause_param
| AddContract of add_contract_param
| RemoveContract of remove_contract_param
| UpdateMultisig of address
| UpdateFactory of address
| SetFactory of address