#import "../../factory_interface.mligo" "Factory"
#import "../../../reveal/reveal_interface.mligo" "Reveal"
#include "reveal_controller_interface.mligo"
#include "../common/common_functions.mligo"

let set_oracle_address (param : set_oracle_address_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setOracleAddress" sender_address : set_oracle_address_param contract option) with
            | None -> (failwith("no setOracleAddress entrypoint") : operation list)
            | Some set_oracle_address_entrypoint -> [Tezos.transaction param 0mutez set_oracle_address_entrypoint] in
        (prepare_multisig "setOracleAddress" param func store), store
    else
        let {
            set_oracle_address_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setOracleAddress" set.revealer : Reveal.set_oracle_address_param contract option) with
        | None -> (failwith("no setOracleAddress for this revealer") : return)
        | Some contr -> [Tezos.transaction set_oracle_address_param 0mutez contr], store

let set_free_metadata (param : set_free_metadata_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setFreeMetadata" sender_address : set_free_metadata_param contract option) with
            | None -> (failwith("no setFreeMetadata entrypoint") : operation list)
            | Some set_free_metadata_entrypoint -> [Tezos.transaction param 0mutez set_free_metadata_entrypoint] in
        (prepare_multisig "setFreeMetadata" param func store), store
    else
        let {
            set_free_metadata_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setFreeMetadata" set.revealer : Reveal.set_free_metadata_param contract option) with
        | None -> (failwith("no setFreeMetadata for this revealer") : return)
        | Some contr -> [Tezos.transaction set_free_metadata_param 0mutez contr], store

let set_token_id_to_metadata_id (param : set_token_id_to_metadata_id_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setTokenIdToMetadataId" sender_address : set_token_id_to_metadata_id_param contract option) with
            | None -> (failwith("no setTokenIdToMetadataId entrypoint") : operation list)
            | Some set_token_id_to_metadata_id_entrypoint -> [Tezos.transaction param 0mutez set_token_id_to_metadata_id_entrypoint] in
        (prepare_multisig "setTokenIdToMetadataId" param func store), store
    else
        let {
            set_token_id_to_metadata_id_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setTokenIdToMetadataId" set.revealer : Reveal.set_token_id_to_metadata_id_param contract option) with
        | None -> (failwith("no setTokenIdToMetadataId for this revealer") : return)
        | Some contr -> [Tezos.transaction set_token_id_to_metadata_id_param 0mutez contr], store

let set_random_offset (param : set_random_offset_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setRandomOffset" sender_address : set_random_offset_param contract option) with
            | None -> (failwith("no setRandomOffset entrypoint") : operation list)
            | Some set_random_offset_entrypoint -> [Tezos.transaction param 0mutez set_random_offset_entrypoint] in
        (prepare_multisig "setRandomOffset" param func store), store
    else
        let {
            set_random_offset_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setRandomOffset" set.revealer : Reveal.set_random_offset_param contract option) with
        | None -> (failwith("no setRandomOffset for this revealer") : return)
        | Some contr -> [Tezos.transaction set_random_offset_param 0mutez contr], store

let add_reveal_admin (param : add_reveal_admin_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%addRevealAdmin" sender_address : add_reveal_admin_param contract option) with
            | None -> (failwith("no addRevealAdmin entrypoint") : operation list)
            | Some add_reveal_admin_entrypoint -> [Tezos.transaction param 0mutez add_reveal_admin_entrypoint] in
        (prepare_multisig "addRevealAdmin" param func store), store
    else
        let {
            add_reveal_admin_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%addRevealAdmin" set.revealer : Reveal.add_reveal_admin_param contract option) with
        | None -> (failwith("no addRevealAdmin for this revealer") : return)
        | Some contr -> [Tezos.transaction add_reveal_admin_param 0mutez contr], store

let remove_reveal_admin (param : remove_reveal_admin_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%removeRevealAdmin" sender_address : remove_reveal_admin_param contract option) with
            | None -> (failwith("no removeRevealAdmin entrypoint") : operation list)
            | Some remove_reveal_admin_entrypoint -> [Tezos.transaction param 0mutez remove_reveal_admin_entrypoint] in
        (prepare_multisig "removeRevealAdmin" param func store), store
    else
        let {
            remove_reveal_admin_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%removeRevealAdmin" set.revealer : Reveal.remove_reveal_admin_param contract option) with
        | None -> (failwith("no removeRevealAdmin for this revealer") : return)
        | Some contr -> [Tezos.transaction remove_reveal_admin_param 0mutez contr], store

let set_ipfs_hashes (param : set_ipfs_hashes_param) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%setIpfsHashes" sender_address : set_ipfs_hashes_param contract option) with
            | None -> (failwith("no setIpfsHashes entrypoint") : operation list)
            | Some set_ipfs_hashes_param_entrypoint -> [Tezos.transaction param 0mutez set_ipfs_hashes_param_entrypoint] in
        (prepare_multisig "setIpfsHashes" param func store), store
    else
        let {
            set_ipfs_hashes_param;
            set_id;
        } = param in
        let set = get_set set_id store in
        match (Tezos.get_entrypoint_opt "%setIpfsHashes" set.revealer : Reveal.set_ipfs_hashes_param contract option) with
        | None -> (failwith("no setIpfsHashes for this revealer") : return)
        | Some contr -> [Tezos.transaction set_ipfs_hashes_param 0mutez contr], store

let update_multisig (param : address) (store : storage) : return =
    if (Tezos.get_sender () <> store.factory) then
        (failwith("only factory can call this entrypoint") : return)
    else
        ([] : operation list), {store with multisig = param}

let update_factory (param : address) (store : storage) : return =
    if (Tezos.get_sender ()) <> store.multisig then
        let sender_address = (Tezos.get_self_address ()) in
        let func () =
            match (Tezos.get_entrypoint_opt "%updateFactory" sender_address : address contract option) with
            | None -> (failwith("no updateFactory entrypoint") : operation list)
            | Some update_factory_entrypoint -> [Tezos.transaction param 0mutez update_factory_entrypoint] in
        (prepare_multisig "updateFactory" param func store), store
    else
        ([] : operation list), {store with factory = param}

let main (action, store : parameter * storage) : return =
match action with
| SetOracleAddress p -> set_oracle_address p store
| SetFreeMetadata p -> set_free_metadata p store
| SetTokenIdToMetadataId p -> set_token_id_to_metadata_id p store
| SetRandomOffset p -> set_random_offset p store
| AddRevealAdmin p -> add_reveal_admin p store
| RemoveRevealAdmin p -> remove_reveal_admin p store
| SetIpfsHashes p -> set_ipfs_hashes p store
| UpdateMultisig p -> update_multisig p store
| UpdateFactory p -> update_factory p store