#import "../mint/mint_interface.mligo" "Mint"
#import "../reveal/reveal_interface.mligo" "Reveal"
#import "../nft/nft.mligo" "Nft"
#import "launchers/mint_launcher/mint_launcher.mligo" "MintLauncher"
#import "launchers/reveal_launcher/reveal_launcher.mligo" "RevealLauncher"
#import "launchers/nft_launcher/nft_launcher.mligo" "NftLauncher"

type entrypoint_signature =
[@layout:comb]
{
    name : string;
    params : bytes;
    source_contract : address;
}

type call_param =
[@layout:comb]
{
    entrypoint_signature : entrypoint_signature;
    callback : unit -> operation list;
}

type minter_storage = Mint.storage
type name_list = Mint.name_list
type whitelist = Mint.whitelist
type revealer_storage = Reveal.storage
type free_metadata = Reveal.free_metadata
type token_id_to_metadata_id = Reveal.token_id_to_metadata_id
type nft_storage = Nft.nft_token_storage
type ledger = Nft.ledger
type operator_storage = Nft.operator_storage
type metadata = Nft.metadata
type token_metadata_storage = Nft.token_metadata_storage
type add_contract_param = Nft.add_contract_param

type set_id = string

(* launch types *)

type update_launchers_param =
[@layout:comb]
{
    mint_launcher : address;
    reveal_launcher : address;
    nft_launcher : address;
}

type update_controllers_param =
[@layout:comb]
{
    mint_controller : address;
    reveal_controller : address;
    nft_controller : address;
}

type after_minter =
[@layout:comb]
{
    set_id : set_id;
    minter_address : address;
}

type after_revealer =
[@layout:comb]
{
    set_id : set_id;
    revealer_address : address;
}

type after_nft =
[@layout:comb]
{
    set_id : set_id;
    nft_address : address;
}

type deploy_set_param = 
| Deploy of set_id
| AfterMinter of after_minter
| AfterRevealer of after_revealer
| AfterNft of after_nft

type reveal_launcher_param = 
[@layout:comb]
{
    revealer_storage : Reveal.storage;
    set_id : set_id;
}

type nft_launcher_param =
[@layout:comb]
{
    nft_storage : nft_storage;
    set_id : set_id;
}

(* general admin types *)

type choose_contract =
| Minter
| Revealer
| Both


type update_method =
| Single of set_id
| Multiple of set_id list

type set_pause_param =
[@layout:comb]
{
    update_method : update_method;
    choose_contract : choose_contract;
    pause : bool;
}

type set_nft_address_param =
[@layout:comb]
{
    set_id : set_id;
    nft_address : address;
}

type deployed_set =
[@layout:comb]
{
    minter : address;
    revealer : address;
    nft : address;
}

type add_nft_contract_param =
[@layout:comb]
{
    nft_address : address;
    contract_addresses : address list;
}

type set_default_reserve_param = address

type set_doga_address_param = address

type set_oracle_address_param = address

type set_minter_address_param =
[@layout:comb]
{
    set_id : set_id;
    minter_address : address;
}

type set_revealer_address_param =
[@layout:comb]
{
    set_id : set_id;
    revealer_address : address;
}



type storage = 
[@layout:comb]
{
    last_deployed_contracts : deployed_set;
    contract_sets : (set_id, deployed_set) big_map;
    multisig : address;
    doga_address : address;
    default_reserve : address;
    default_oracle : address;
    mint_launcher : address;
    reveal_launcher : address;
    nft_launcher : address;
    mint_controller : address;
    reveal_controller : address;
    nft_controller : address;
}

type return = operation list * storage

type parameter =
| DeploySet of deploy_set_param
| UpdateMultisig of address
| SetPause of set_pause_param
| SetNftAddress of set_nft_address_param
| SetDogaAddress of set_doga_address_param
| SetOracleAddress of set_oracle_address_param
| SetDefaultReserve of set_default_reserve_param
| SetRevealerAddress of set_revealer_address_param
| SetMinterAddress of set_minter_address_param
| UpdateLaunchers of update_launchers_param
| UpdateControllers of update_controllers_param

