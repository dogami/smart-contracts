#if !MINT_LAUNCHER
#define MINT_LAUNCHER

type storage = 
[@layout:comb]
{
    factory : address;
    controller : address;
}
type return = operation list * storage

#import "../../../mint/mint.mligo" "Mint"
#import "../../../nft/nft.mligo" "Nft"
// #import "factory.mligo" "Factory"

type set_id = string

let launch_minter (initial_storage : Mint.storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../../../michelson/mint.tz"
  ;
           PAIR } |} : 
       (key_hash option * tez * Mint.storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)

type launcher_parameter = 
[@layout:comb]
{
    doga_address : address;
    reserve : address;
    set_id : set_id;
}

type after_minter =
[@layout:comb]
{
    set_id : set_id;
    minter_address : address;
}

type after_revealer =
[@layout:comb]
{
    set_id : set_id;
    revealer_address : address;
}

type after_nft =
[@layout:comb]
{
    set_id : set_id;
    nft_address : address;
}

type deploy_set_param = 
| Deploy of set_id
| AfterMinter of after_minter
| AfterRevealer of after_revealer
| AfterNft of after_nft

let main ((param, store) : launcher_parameter * storage) : return =
    if (Tezos.get_sender ()) <> store.factory then
        (failwith("only factory can call") : return)
    else
        let { doga_address; reserve; set_id } = param in
        let minter_storage =
            {
                  paused = false;
                  controller = store.controller;
                  factory = store.factory;
                  nft_address = (Tezos.get_self_address());
                  doga_address = doga_address;
                  reserve_address = reserve;
                  is_private_sale = false;
                  mint_price = 0n;
                  total_supply = 0n;
                  mint_per_address = (Big_map.empty : Mint.mint_per_address);
                  max_mint_per_address = 0n;
                  start_time = (Tezos.get_now ());
                  end_time = (Tezos.get_now ());
                  next_token_id = 0n;
                  name_list = (Big_map.empty : Mint.name_list);
                  whitelist = (Big_map.empty : Mint.whitelist);
                  token_per_pack = 0n;
                  default_metadata = (Map.empty : Nft.token_metadata);
                  currency = "";
            } in
        let op_launch_minter, minter_address = launch_minter minter_storage in
        let factory_deploy_set_contract = 
            match (Tezos.get_entrypoint_opt "%deploySet" store.factory : deploy_set_param contract option) with
            | None -> (failwith("no deploySet entrypoint for factory") : deploy_set_param contract)
            | Some contr -> contr in
        let continue_deployment_param =
            {
                minter_address = minter_address;
                set_id = set_id;
            } in
        let op_continue_deployment = Tezos.transaction (AfterMinter continue_deployment_param) 0mutez factory_deploy_set_contract in
        [op_launch_minter; op_continue_deployment], store

#endif