#if !NFT_LAUNCHER
#define NFT_LAUNCHER

type storage =
[@layout:comb]
{
    factory : address;
    controller : address;
}

type return = operation list * storage

#import "../../../nft/nft.mligo" "Nft"

type set_id = string

type after_minter =
[@layout:comb]
{
    set_id : set_id;
    minter_address : address;
}

type after_revealer =
[@layout:comb]
{
    set_id : set_id;
    revealer_address : address;
}

type after_nft =
[@layout:comb]
{
    set_id : set_id;
    nft_address : address;
}

type deploy_set_param =
| Deploy of set_id
| AfterMinter of after_minter
| AfterRevealer of after_revealer
| AfterNft of after_nft

let launch_nft (initial_storage : Nft.nft_token_storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../../../michelson/nft.tz"
  ;
           PAIR } |} :
       (key_hash option * tez * Nft.nft_token_storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)

type launcher_param =
[@layout:comb]
{
    minter_address : address;
    revealer_address : address;
    set_id : set_id;
}

let main ((param, store) : launcher_param * storage) : return =
    if (Tezos.get_sender ()) <> store.factory then
        (failwith("only factory can call") : return)
    else
        let { minter_address; revealer_address; set_id } = param in
        let nft_storage =
            {
                controller = store.controller;
                factory = store.factory;
                contract_paused = false;
                burn_paused = false;
                total_supply = 0n;
                contracts = Set.literal [minter_address; revealer_address];
                ledger = (Big_map.empty : Nft.ledger);
                operators = (Big_map.empty : Nft.operator_storage);
                metadata = (Big_map.empty : Nft.metadata);
                token_metadata = (Big_map.empty : Nft.token_metadata_storage);
            } in
        let op_launch_nft, nft_address = launch_nft nft_storage in
        let factory_deploy_set_contract =
            match (Tezos.get_entrypoint_opt "%deploySet" store.factory : deploy_set_param contract option) with
            | None -> (failwith("no deploySet entrypoint for factory") : deploy_set_param contract)
            | Some contr -> contr in
        let continue_deployment_param =
            {
                nft_address = nft_address;
                set_id = set_id;
            } in
        let op_continue_deployment = Tezos.transaction (AfterNft continue_deployment_param) 0mutez factory_deploy_set_contract in
        [op_launch_nft; op_continue_deployment], store

#endif