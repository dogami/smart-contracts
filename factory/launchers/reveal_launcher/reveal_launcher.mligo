#if !REVEAL_LAUNCHER
#define REVEAL_LAUNCHER

type storage = 
[@layout:comb]
{
    factory : address;
    controller : address;
}
type return = operation list * storage

#import "../../../reveal/reveal.mligo" "Reveal"
#import "../../../nft/nft.mligo" "Nft"
// #import "factory.mligo" "Factory"

type set_id = string

type after_minter =
[@layout:comb]
{
    set_id : set_id;
    minter_address : address;
}

type after_revealer =
[@layout:comb]
{
    set_id : set_id;
    revealer_address : address;
}

type after_nft =
[@layout:comb]
{
    set_id : set_id;
    nft_address : address;
}

type deploy_set_param = 
| Deploy of set_id
| AfterMinter of after_minter
| AfterRevealer of after_revealer
| AfterNft of after_nft

let launch_revealer (initial_storage : Reveal.storage) : operation * address =
([%Michelson
    ({| {  UNPPAIIR ;
           CREATE_CONTRACT
#include "../../../michelson/reveal.tz"
  ;
           PAIR } |} : 
       (key_hash option * tez * Reveal.storage) -> (operation * address))])
    ((None : key_hash option), 0mutez, initial_storage)

type launcher_param = 
[@layout:comb]
{
    oracle : address;
    set_id : set_id;
}

let main ((param, store) : launcher_param * storage) : return =
    if (Tezos.get_sender ()) <> store.factory then
        (failwith("only factory can call") : return)
    else
        let { oracle; set_id } = param in
        let revealer_storage =
            {
                paused = false;
                controller = store.controller;
                factory = store.factory;
                nft_address = (Tezos.get_self_address());
                reveal_time = (Tezos.get_now ());
                free_metadata_v1 = (Big_map.empty : Reveal.free_metadata);
                free_metadata_v1_length = 0n;
                free_metadata_v2 = (Big_map.empty : Reveal.free_metadata);
                free_metadata_v2_length = 0n;
                // free_metadatas: (drop_number, free_metadata) map;
                // free_metadata_lengths: (drop_number, nat) map;
                token_id_to_metadata_id = (Big_map.empty : Reveal.token_id_to_metadata_id);
                random_offset = 0n;
                reveal_admins = (Set.empty : address set);
                oracle = oracle;
                ipfs_hashes = "";
            } in
        let op_launch_revealer, revealer_address = launch_revealer revealer_storage in
        let factory_deploy_set_contract = 
            match (Tezos.get_entrypoint_opt "%deploySet" store.factory : deploy_set_param contract option) with
            | None -> (failwith("no deploySet entrypoint for factory") : deploy_set_param contract)
            | Some contr -> contr in
        let continue_deployment_param =
            {
                revealer_address = revealer_address;
                set_id = set_id;
            } in
        let op_continue_deployment = Tezos.transaction (AfterRevealer continue_deployment_param) 0mutez factory_deploy_set_contract in
        [op_launch_revealer; op_continue_deployment], store

#endif