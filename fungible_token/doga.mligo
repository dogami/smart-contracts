type transfer =
[@layout:comb] {
  [@annot:from] address_from : address;
  [@annot:to] address_to : address;
  value : nat
}

type transfer_batch = transfer list

type approve =
[@layout:comb] {
  spender : address;
  value : nat
}

type allowance_key =
[@layout:comb] {
  owner : address;
  spender : address
}

type get_allowance =
[@layout:comb] {
  request : allowance_key;
  callback : nat contract
}

type get_balance =
[@layout:comb] {
  owner : address;
  callback : nat contract
}

type get_total_supply =
[@layout:comb] {
  request : unit ; // why this ?
  callback : nat contract
}

type burn =
[@layout:comb] {
  address_from : address;
  value : nat
}

type set_token_metadata =
[@layout:comb] {
  uri : string;
  name : string;
  symbol : string;
  decimals : nat;
  icon : string;
}

type token_id = nat
type tokens = (address, nat) big_map
type allowances = (allowance_key, nat) big_map

type token_metadata = (string, bytes) map
type token_metadata_item =
[@layout:comb] {
  token_id: token_id;
  metadata: token_metadata;
}
type token_metadata_storage = (token_id, token_metadata_item) big_map

type storage = {
  admin: address;
  paused: bool;
  tokens : tokens;
  allowances : allowances;
  total_supply : nat;
  token_metadata: token_metadata_storage;
}

type parameter =
  | Transfer of transfer
  | TransferBatch of transfer_batch
  | Approve of approve
  | Burn of burn
  | GetAllowance of get_allowance
  | GetBalance of get_balance
  | GetTotalSupply of get_total_supply
  | SetPause of bool
  | SetAdmin of address
  | SetTokenMetadata of set_token_metadata

type result = operation list * storage

[@inline]
let positive (n : nat) : nat option =
  if n = 0n
    then (None : nat option)
  else
    Some n

let transfer (param, storage : transfer * storage) : storage =
  if storage.paused = true then
    (failwith("contract in pause") : storage)
  else
    let allowances = storage.allowances in
    let tokens = storage.tokens in

    let allowances =
      if Tezos.sender = param.address_from then allowances
      else
        let allowance_key = { owner = param.address_from ; spender = Tezos.sender } in
        let authorized_value = match Big_map.find_opt allowance_key allowances with
          | Some value -> value
          | None -> 0n in
        let authorized_value = match is_nat (authorized_value - param.value) with
          | None -> (failwith "NotEnoughAllowance" : nat)
          | Some authorized_value -> authorized_value in
        Big_map.update allowance_key (positive authorized_value) allowances in

    let tokens =
      let from_balance = match Big_map.find_opt param.address_from tokens with
        | Some value -> value
        | None -> 0n in
      let from_balance = match is_nat (from_balance - param.value) with
        | None -> (failwith "NotEnoughBalance" : nat)
        | Some from_balance -> from_balance in
      Big_map.update param.address_from (positive from_balance) tokens in

    let tokens =
      let to_balance = match Big_map.find_opt param.address_to tokens with
        | Some value -> value
        | None -> 0n in
      let to_balance = to_balance + param.value in
      Big_map.update param.address_to (positive to_balance) tokens in
    { storage with tokens = tokens; allowances = allowances }

let transfer_batch (param, storage : transfer_batch * storage) : result =
  if storage.paused = true then
    (failwith("contract in pause") : result)
  else
    let transfer_single (st, tr : storage * transfer) : storage =
      transfer (tr, st)
    in
    let new_storage = List.fold transfer_single param storage in
    (([] : operation list), new_storage)

let approve (param, storage : approve * storage) : result =
  if storage.paused = true then
    (failwith("contract in pause") : result)
  else
    let allowances = storage.allowances in
    let allowance_key = { owner = Tezos.sender ; spender = param.spender } in
    let previous_value =
      match Big_map.find_opt allowance_key allowances with
      | Some value -> value
      | None -> 0n
    in
    begin
      if previous_value > 0n && param.value > 0n
        then (failwith "UnsafeAllowanceChange")
      else ();
      let allowances = Big_map.update allowance_key (positive param.value) allowances in
      (([] : operation list), { storage with allowances = allowances })
    end

let burn (param, storage : burn * storage): result =
  if Tezos.sender = storage.admin then
    let tokens =
      let from_balance =
        match Big_map.find_opt param.address_from storage.tokens with
        | Some value -> value
        | None -> 0n
      in
      let from_balance =
        match is_nat (from_balance - param.value) with
        | None -> (failwith "NotEnoughBalance" : nat)
        | Some from_balance -> from_balance
      in
      Big_map.update param.address_from (positive from_balance) storage.tokens
      in
      let new_total_supply =
        match is_nat (storage.total_supply - param.value) with
        | None -> (failwith "NotEnoughBalance" : nat)
        | Some new_total_supply -> new_total_supply
      in
    (([] : operation list), { storage with tokens = tokens; total_supply = new_total_supply })
  else 
    (failwith("only admin can do it") : result)

let get_allowance (param, storage : get_allowance * storage) : result =
  let value =
    match Big_map.find_opt param.request storage.allowances with
    | Some value -> value
    | None -> 0n in
  [Tezos.transaction value 0mutez param.callback], storage

let get_balance (param, storage : get_balance * storage) : result =
  let value =
    match Big_map.find_opt param.owner storage.tokens with
    | Some value -> value
    | None -> 0n in
  [Tezos.transaction value 0mutez param.callback], storage

let get_total_supply (param, storage : get_total_supply * storage) : result =
  let total = storage.total_supply in
  [Tezos.transaction total 0mutez param.callback], storage

let set_pause (param, storage : bool * storage): result =
  if Tezos.sender = storage.admin then
    (([] : operation list), { storage with paused = param })
  else
    (failwith("only admin can do it") : result)

let set_admin (param, storage : address * storage): result =
  if Tezos.sender = storage.admin then
    (([] : operation list), { storage with admin = param })
  else 
    (failwith("only admin can do it") : result)

let set_token_metadata (param, storage : set_token_metadata * storage): result =
  if Tezos.sender = storage.admin then
    let token_id = 0n in
    let token_info = Map.literal [
      ("", Bytes.pack param.uri);
      ("name", Bytes.pack param.name);
      ("symbol", Bytes.pack param.symbol);
      ("decimals", Bytes.pack param.decimals);
      ("icon", Bytes.pack param.icon);
    ] in
    let new_token_metadata_entry = {
      token_id = token_id;
      metadata = token_info
    } in
    let new_token_metadata = Big_map.update token_id (Some new_token_metadata_entry) storage.token_metadata in
    (([] : operation list), { storage with token_metadata = new_token_metadata })
  else
    (failwith("only admin can do it") : result)

let main (param, storage : parameter * storage) : result =
  begin
    if Tezos.amount <> 0mutez
      then failwith "DontSendTez"
    else ();
    match param with
    | Transfer param -> ([] : operation list), transfer (param, storage)
    | TransferBatch param -> transfer_batch (param, storage)
    | Approve param -> approve (param, storage)
    | Burn param -> burn (param, storage)
    | GetAllowance param -> get_allowance (param, storage)
    | GetBalance param -> get_balance (param, storage)
    | GetTotalSupply param -> get_total_supply (param, storage)
    | SetPause param -> set_pause (param, storage)
    | SetAdmin param -> set_admin (param, storage)
    | SetTokenMetadata param -> set_token_metadata (param, storage)
  end
