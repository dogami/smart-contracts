#if !MINT_CONSTANTS
#define MINT_CONSTANTS

[@inline] let error_NFT_CONTRACT_MUST_HAVE_A_MINT_ENTRYPOINT = "NFT contract must have a mint entrypoint"
[@inline] let error_NFT_CONTRACT_MUST_HAVE_A_BURN_ENTRYPOINT = "NFT contract must have a burn entrypoint"
[@inline] let error_INVALID_TO_ADDRESS = "Invalid to address"
[@inline] let error_TOTAL_SUPPLY_REACHED = "Total supply reached"
[@inline] let error_INVALID_NUMBER_OF_TOKENS = "Invalid number of tokens"
[@inline] let error_MAX_MINT_PER_ADDRESS_REACHED = "Max mint per address reached"
[@inline] let error_ADDRESS_IS_NOT_WHITELISTED = "Address is not whitelisted"
[@inline] let error_MINT_PERIOD_IS_NOT_OPEN = "Mint period is not open"
[@inline] let error_INVALID_AMOUNT = "Invalid amount"
[@inline] let error_INVALID_CURRENCY = "Invalid currency"
[@inline] let error_VALUE_SHOULD_BE_HIGHER_THAN_ZERO = "Value should be higher than zero"
[@inline] let error_CONTRACT_IN_PAUSE = "Contract in pause"
[@inline] let error_INVALID_DATE_FORMAT = "Invalid date format"
[@inline] let error_INVALID_NAT_VALUE = "Invalid nat value"
[@inline] let error_MULTISIG_ALREADY_SET = "Multisig already set"

#endif