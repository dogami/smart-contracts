#if !MINT_INTERFACE
#define MINT_INTERFACE

#include "../nft/fa2_interface.mligo"

type fa12_contract_transfer =
[@layout:comb]
{ [@annot:from] address_from : address;
  [@annot:to] address_to : address;
  value : nat;
}

type entrypoint_signature =
[@layout:comb]
{
    name : string;
    params : bytes;
    source_contract : address;
}

type call_param =
[@layout:comb]
{
    entrypoint_signature : entrypoint_signature;
    callback : unit -> operation list;
}

(* storage types *)
type whitelist = (address, bool) big_map
type mint_per_address = (address, nat) big_map
type metadata_id = nat
type name_list = (nat, bytes) big_map

type storage =
[@layout:comb]
{
  paused: bool;
  controller : address;
  factory : address;
  nft_address: address;
  doga_address: address;
  reserve_address: address;
  is_private_sale: bool;
  mint_price: nat;
  total_supply: nat;
  mint_per_address: mint_per_address;
  max_mint_per_address: nat;
  start_time: timestamp;
  end_time: timestamp;
  next_token_id: token_id;
  name_list: name_list;
  whitelist: whitelist;
  token_per_pack: nat; (* number of nfts that must be minted together *)
  default_metadata: token_metadata;
  currency: string;
}

(* param types *)
type set_pause_param = bool
type set_nft_address_param = address
type set_doga_address_param = address
type set_reserve_address_param = address
type update_Multisig_param = address
type set_mint_params_param =
[@layout:comb]
{
  is_private_sale: bool;
  mint_price: nat;
  currency: string;
  next_token_id: nat;
  total_supply: nat;
  max_mint_per_address: nat;
  token_per_pack: nat;
  start_time: timestamp;
  end_time: timestamp;
}
type add_to_whitelist_param = (address * bool) list
type remove_from_whitelist_param = address list
type set_default_metadata_param = token_metadata
type mint_from_crowdsale_param = {
  token_count: nat;
  to_: address;
}
type burn_from_crowdsale_param = burn_param
type acc = {
  operation_list: operation list;
  storage: storage;
}
type set_name_list_param = (nat, bytes) map

type return = operation list * storage

type parameter =
| SetPause of set_pause_param
| SetNftAddress of set_nft_address_param
| SetDogaAddress of set_doga_address_param
| SetReserveAddress of set_reserve_address_param
| SetMintParams of set_mint_params_param
| SetMintPerAddress
| AddToWhitelist of add_to_whitelist_param
| RemoveFromWhitelist of remove_from_whitelist_param
| SetDefaultMetadata of set_default_metadata_param
| MintFromCrowdsale of mint_from_crowdsale_param
| BurnFromCrowdsale of burn_from_crowdsale_param
| SetNameList of set_name_list_param

#endif
