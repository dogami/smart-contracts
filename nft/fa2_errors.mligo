#if !FA2_ERRORS
#define FA2_ERRORS

(** One of the specified `token_id`s is not defined within the FA2 contract *)
let fa2_token_undefined = "FA2_TOKEN_UNDEFINED"
(**
A token owner does not have sufficient balance to transfer tokens from
owner's account
*)
let fa2_insufficient_balance = "FA2_INSUFFICIENT_BALANCE"
(**
A transfer failed because `operator_transfer_policy == Owner_transfer` and it is
initiated not by the token owner
*)
let fa2_not_owner = "FA2_NOT_OWNER"
(**
A transfer failed because `operator_transfer_policy == Owner_or_operator_transfer`
and it is initiated neither by the token owner nor a permitted operator
 *)
let fa2_not_operator = "FA2_NOT_OPERATOR"

[@inline] let error_ONLY_ADMIN_CAN_CALL_THIS_ENTRYPOINT = "Only admin can call this entrypoint"
[@inline] let error_UNAUTHORIZED_CONTRACT_ADDRESS = "Unauthorized contract address"
[@inline] let error_INVALID_TO_ADDRESS = "Invalid to address"
[@inline] let error_TOKEN_ID_DOES_NOT_EXIST = "Token id does not exist"
[@inline] let error_TOKEN_METADATA_ITEM_DOES_NOT_EXIST = "Token metadata does not exists"
[@inline] let error_FA2_CONTRACT_IS_PAUSED = "Contract is paused"
[@inline] let error_FA2_BURN_IS_PAUSED = "Burn is paused"
[@inline] let error_FA2_INVALID_BALANCE = "Invalid balance"
[@inline] let error_TOKEN_ALREADY_EXISTS = "Token already exists"
[@inline] let error_INVALID_OWNER_ADDRESS = "Invalid owner address"

#endif
