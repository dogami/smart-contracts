#if ! FA2_INTERFACE
#define FA2_INTERFACE

type token_id = nat
type metadata = (string, bytes) big_map
type token_metadata = (string, bytes) map
type ledger = (token_id, address) big_map

type token_metadata_item =
[@layout:comb]
{
  token_id: token_id;
  token_info: token_metadata;
}
type token_metadata_storage = (token_id, token_metadata_item) big_map

(* param types *)
type add_contract_param = address
type remove_contract_param = address
type set_contract_pause_param = bool
type set_burn_pause_param = bool
type burn_param = 
[@layout:comb]
{
  token_id : nat;
  from_ : address;
}

type nft_mint_param = {
  to_ : address;
  token_id: token_id;
  token_info: token_metadata
}

type transfer_destination =
[@layout:comb]
{
  to_ : address;
  token_id : token_id;
  amount : nat;
}
type transfer =
[@layout:comb]
{
  from_ : address;
  txs : transfer_destination list;
}

type balance_of_request =
[@layout:comb]
{
  owner : address;
  token_id : token_id;
}
type balance_of_response =
[@layout:comb]
{
  request : balance_of_request;
  balance : nat;
}
type balance_of_param =
[@layout:comb]
{
  requests : balance_of_request list;
  callback : (balance_of_response list) contract;
}

type operator_param =
[@layout:comb]
{
  owner : address;
  operator : address;
  token_id: token_id;
}
type update_operator =
[@layout:comb]
  | Add_operator of operator_param
  | Remove_operator of operator_param

type metadata_updater = token_metadata -> token_metadata
type update_metadata_with_function_param = {
  token_id: token_id;
  metadata_updater: metadata_updater;
}

type entrypoint_signature =
[@layout:comb]
{
    name : string;
    params : bytes;
    source_contract : address;
}

type call_param =
[@layout:comb]
{
    entrypoint_signature : entrypoint_signature;
    callback : unit -> operation list;
}

type set_metadata_param = bytes
type update_multisig_param = address

type fa2_entry_points =
  | AddContract of add_contract_param
  | RemoveContract of remove_contract_param
  | SetContractPause of set_contract_pause_param
  | SetBurnPause of set_burn_pause_param
  | Mint of nft_mint_param
  | Transfer of transfer list
  | Burn of burn_param
  | Balance_of of balance_of_param
  | Update_operators of update_operator list
  | UpdateMetadataWithFunction of update_metadata_with_function_param
  | SetMetadata of set_metadata_param
  | SetFactory of address
  | SetController of address

#endif
