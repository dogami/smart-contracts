#include "./fa2_interface.mligo"
#include "./fa2_errors.mligo"
#include "./fa2_operator_lib.mligo"

(*
  keep the definition of the `nft_token_storage` here,
  as `operator_storage` is defined in `fa2_operator_lib`
  which is not included in other contracts
*)
type nft_token_storage = {
  controller : address;
  factory : address;
  contract_paused : bool;
  burn_paused : bool;
  total_supply: nat;
  contracts : address set;
  ledger : ledger;
  operators : operator_storage;
  metadata: metadata;
  token_metadata : token_metadata_storage;
}

type return = operation list * nft_token_storage

let add_contract (param, storage : add_contract_param * nft_token_storage) : return =
  if (Tezos.get_sender ()) <> storage.controller && (Tezos.get_sender ()) <> storage.factory then
      (failwith("only controller or factory can call this entrypoint") : return)
  else
      ([] : operation list), { storage with contracts = Set.add param storage.contracts }

let remove_contract (param, storage : remove_contract_param * nft_token_storage) : return =
  if (Tezos.get_sender ()) <> storage.controller && (Tezos.get_sender ()) <> storage.factory then
      (failwith("only controller or factory can call this entrypoint") : return)
  else
      ([] : operation list), { storage with contracts = Set.remove param storage.contracts }

let set_contract_pause (param, storage : set_contract_pause_param * nft_token_storage) : return =
  if (Tezos.get_sender ()) <> storage.controller then
      (failwith("only controller can call this entrypoint") : return)
  else
      ([] : operation list), { storage with contract_paused = param }

let set_burn_pause (param, storage : set_burn_pause_param * nft_token_storage) : return =
  if (Tezos.get_sender ()) <> storage.controller then
    (failwith("only controller can call this entrypoint"))
  else
    ([] : operation list), { storage with burn_paused = param }

let mint (param, storage : nft_mint_param * nft_token_storage) : nft_token_storage =
  if storage.contract_paused then
    failwith error_FA2_CONTRACT_IS_PAUSED
  else if (not Set.mem (Tezos.get_sender ()) storage.contracts) then
    failwith error_UNAUTHORIZED_CONTRACT_ADDRESS
  else
    let { to_ = to_;
          token_id = token_id;
          token_info = token_info } = param
    in
    let _check_if_token_already_exists = match Big_map.find_opt token_id storage.ledger with
      | Some _v -> failwith error_TOKEN_ALREADY_EXISTS
      | None -> ()
    in
    let new_ledger = Big_map.update token_id (Some to_) storage.ledger
    in
    let nft_metadata = {
      token_id = token_id;
      token_info = token_info;
    } in
    let new_token_metadata = Big_map.update
      token_id
      (Some nft_metadata)
      storage.token_metadata
    in
    let new_total_supply = storage.total_supply + 1n in
    { storage with ledger = new_ledger ; token_metadata = new_token_metadata ; total_supply = new_total_supply }

let transfer (txs, validate_op, ops_storage, ledger
    : (transfer list) * operator_validator * operator_storage * ledger) : ledger =
  (* process individual transfer *)
  let make_transfer = (fun (l, tx : ledger * transfer) ->
    List.fold
      (fun (ll, dst : ledger * transfer_destination) ->
        if dst.amount = 0n
          then ll
        else if dst.amount <> 1n
          then (failwith fa2_insufficient_balance : ledger)
        else
          let owner = Big_map.find_opt dst.token_id ll in
          match owner with
          | None -> (failwith fa2_token_undefined : ledger)
          | Some o ->
            if o <> tx.from_
              then (failwith fa2_insufficient_balance : ledger)
            else
              let _u = validate_op (o, (Tezos.get_sender ()), dst.token_id, ops_storage) in
              Big_map.update dst.token_id (Some dst.to_) ll
      ) tx.txs l
  )
  in
  List.fold make_transfer txs ledger

let burn (param, storage : burn_param * nft_token_storage) : nft_token_storage =
  if storage.burn_paused = true then
    failwith error_FA2_BURN_IS_PAUSED
  else if (not Set.mem (Tezos.get_sender ()) storage.contracts) then
    failwith error_UNAUTHORIZED_CONTRACT_ADDRESS
  else
    let { token_id = token_id; from_ = from_ } = param in

    let new_ledger = match Big_map.find_opt token_id storage.ledger with
    | None -> (failwith error_TOKEN_ID_DOES_NOT_EXIST : ledger)
    | Some owner ->
      if owner <> from_
        then (failwith error_INVALID_OWNER_ADDRESS : ledger)
      else
        let _u = default_operator_validator (owner, (Tezos.get_sender ()), token_id, storage.operators) in
        Big_map.update token_id (None : address option) storage.ledger
    in

    let new_token_metadata =
      Big_map.update token_id (None : token_metadata_item option) storage.token_metadata
    in

    let new_total_supply =
      match is_nat (storage.total_supply - 1n) with
      | None -> 0n
      | Some new_total_supply -> new_total_supply
    in
    { storage with ledger = new_ledger; token_metadata = new_token_metadata; total_supply = new_total_supply}

let get_balance (p, ledger : balance_of_param * ledger) : operation =
  let to_balance = fun (r : balance_of_request) ->
    let owner = Big_map.find_opt r.token_id ledger in
    match owner with
    | None -> (failwith fa2_token_undefined : balance_of_response)
    | Some o ->
      let bal = if o = r.owner then 1n else 0n in
      { request = r; balance = bal; }
  in
  let responses = List.map to_balance p.requests in
  Tezos.transaction responses 0mutez p.callback

let update_metadata_with_function (param : update_metadata_with_function_param) (storage : nft_token_storage) : nft_token_storage =
  let { token_id = token_id ;
        metadata_updater = metadata_updater } = param in
  if (not Set.mem (Tezos.get_sender ()) storage.contracts) then
    failwith error_UNAUTHORIZED_CONTRACT_ADDRESS
  else
    match Big_map.find_opt token_id storage.token_metadata with
    | None -> (failwith error_TOKEN_METADATA_ITEM_DOES_NOT_EXIST : nft_token_storage)
    | Some token_metadata_entry ->
      let new_token_metadata_entry = {
        token_id = token_id;
        token_info = metadata_updater token_metadata_entry.token_info;
      } in
      let new_token_metadata = Big_map.update token_id (Some new_token_metadata_entry) storage.token_metadata in
      { storage with token_metadata = new_token_metadata }

let set_factory (param : address) (store : nft_token_storage) : return =
  if Tezos.get_sender () <> store.controller then
    (failwith("only controller can call this entrypoint nft-set_factory") : return)
  else
    ([] : operation list), {store with factory = param}

let set_controller (param : address) (store : nft_token_storage) : return =
  if Tezos.get_sender () <> store.factory then
   (failwith("only factory can call this entrypoint nft-set_controller") : return)
  else
    ([] : operation list), {store with controller = param}

[@view] let balance_of_view (param, s: balance_of_request * nft_token_storage) : nat =
  let owner = Big_map.find_opt param.token_id s.ledger in
  match owner with
  | None -> (failwith fa2_token_undefined : nat)
  | Some o ->
    let bal = if o = param.owner then 1n else 0n in
    bal

[@view] let total_supply_view (_param, s: unit * nft_token_storage) : nat =
  s.total_supply

let set_metadata (param, storage : set_metadata_param * nft_token_storage): return =
  if (Tezos.get_sender ()) <> storage.controller then
    (failwith("only controller can call this entrypoint") : return)
  else
    let metadata_content = Big_map.update "content" (Some param) storage.metadata in
    ([] : operation list), { storage with metadata = metadata_content }


let main (param, storage : fa2_entry_points * nft_token_storage)
    : (operation  list) * nft_token_storage =
  match param with
  | AddContract new_contract ->
    add_contract (new_contract, storage)
  | RemoveContract contract ->
    remove_contract (contract, storage)
  | SetContractPause pause ->
    set_contract_pause (pause, storage)
  | SetBurnPause pause ->
    set_burn_pause (pause, storage)
  | Mint param ->
    ([] : operation list), mint (param, storage)
  | Transfer txs ->
    if storage.contract_paused then
      (failwith (error_FA2_CONTRACT_IS_PAUSED) : (operation  list) * nft_token_storage)
    else
      let new_ledger = transfer
        (txs, default_operator_validator, storage.operators, storage.ledger) in
      let new_storage = { storage with ledger = new_ledger; } in
      ([] : operation list), new_storage
  | Burn param ->
    ([] : operation list), burn(param, storage)
  | Balance_of param ->
    let op = get_balance (param, storage.ledger) in
    [op], storage
  | Update_operators updates ->
    let new_ops = fa2_update_operators (updates, storage.operators) in
    let new_storage = { storage with operators = new_ops; } in
    ([] : operation list), new_storage
  | UpdateMetadataWithFunction param ->
    let new_storage = update_metadata_with_function param storage in
    ([] : operation list), new_storage
  | SetMetadata param-> set_metadata (param, storage)
  | SetFactory p -> set_factory p storage
  | SetController p -> set_controller p storage
