from test_env import Env, Keys, FA2Storage

import unittest
from pytezos.rpc.errors import MichelsonError

import logging
logging.basicConfig(level=logging.INFO)

ALICE_PK = Keys().ALICE_PK
BOB_PK = Keys().BOB_PK
RESERVE_PK = Keys().RESERVE_PK

alice_pytezos = Keys().alice_pytezos
bob_pytezos = Keys().bob_pytezos
reserve_pytezos = Keys().reserve_pytezos

send_conf = dict(min_confirmations=1)


class TestFactory(unittest.TestCase):
    @ staticmethod
    def print_title(instance):
        print("Test Factory: " + instance.__class__.__name__ + "...")
        print("-----------------------------------")

    @ staticmethod
    def print_success(function_name):
        print(function_name + "... ok")
        print("-----------------------------------")

    class LaunchSet(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_launches_a_set_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(factory.storage["multisig"](), multisig.address)
            self.assertEqual(factory.storage["doga_address"](), ALICE_PK)
            self.assertEqual(factory.storage["default_reserve"](), ALICE_PK)

            last_deployed_contracts = factory.storage["last_deployed_contracts"](
            )
            self.assertEqual(
                factory.storage["contract_sets"]["set1"](), last_deployed_contracts)

            minter_address = factory.storage["last_deployed_contracts"]["minter"](
            )
            revealer_address = factory.storage["last_deployed_contracts"]["revealer"](
            )
            nft_address = factory.storage["last_deployed_contracts"]["nft"]()
            minter = alice_pytezos.contract(minter_address)
            revealer = alice_pytezos.contract(revealer_address)
            nft = alice_pytezos.contract(nft_address)
            self.assertIn(
                nft_address, multisig.storage["authorized_contracts"]())
            self.assertIn(factory.address,
                          multisig.storage["authorized_contracts"]())
            self.assertEqual(minter.storage["nft_address"](), nft_address)
            self.assertEqual(revealer.storage["nft_address"](), nft_address)
            self.assertIn(revealer_address, nft.storage["contracts"]())
            self.assertIn(minter_address, nft.storage["contracts"]())
            TestFactory.print_success("test01_it_launches_a_set_successfully")

        def test02_it_fails_when_set_name_exists(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "contract set name exists"})
            TestFactory.print_success("test02_it_fails_when_set_name_exists")

        def test03_it_fails_when_mint_launcher_is_not_the_correct_contract_type(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            reveal_launcher = factory.storage["reveal_launcher"]()
            nft_launcher = factory.storage["nft_launcher"]()
            factory.updateLaunchers(
                {"mint_launcher": ALICE_PK, "reveal_launcher": reveal_launcher, "nft_launcher": nft_launcher}).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "mint launcher was not deployed correctly"})
            TestFactory.print_success(
                "test03_it_fails_when_mint_launcher_is_not_the_correct_contract_type")

        def test04_it_fails_when_afterMinter_is_called(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet(
                    {"afterMinter": {"set_id": "set1", "minter_address": ALICE_PK}}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "only mint launcher can call afterMinter"})
            TestFactory.print_success(
                "test04_it_fails_when_afterMinter_is_called")

        def test05_it_fails_when_reveal_launcher_is_not_the_correct_contract_type(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            mint_launcher = factory.storage["mint_launcher"]()
            nft_launcher = factory.storage["nft_launcher"]()
            factory.updateLaunchers(
                {"mint_launcher": mint_launcher, "reveal_launcher": ALICE_PK, "nft_launcher": nft_launcher}).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "reveal launcher was not deployed correctly"})
            TestFactory.print_success(
                "test05_it_fails_when_reveal_launcher_is_not_the_correct_contract_type")

        def test06_it_fails_when_afterRevealer_is_called(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet(
                    {"afterRevealer": {"set_id": "set1", "revealer_address": ALICE_PK}}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "only reveal launcher can call afterRevealer"})
            TestFactory.print_success(
                "test06_it_fails_when_afterRevealer_is_called")

        def test07_it_fails_when_nft_launcher_is_not_the_correct_contract_type(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            mint_launcher = factory.storage["mint_launcher"]()
            reveal_launcher = factory.storage["reveal_launcher"]()
            factory.updateLaunchers(
                {"mint_launcher": mint_launcher, "reveal_launcher": reveal_launcher, "nft_launcher": ALICE_PK}).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "nft launcher was not deployed correctly"})
            TestFactory.print_success(
                "test07_it_fails_when_nft_launcher_is_not_the_correct_contract_type")

        def test08_it_fails_when_afterNft_is_called(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet(
                    {"afterNft": {"set_id": "set1", "nft_address": ALICE_PK}}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "only nft launcher can call afterNft"})
            TestFactory.print_success(
                "test08_it_fails_when_afterNft_is_called")

        def test09_it_fails_when_factory_is_not_authorized_by_multisig(self):
            multisig = Env().deploy_multisig()
            factory = Env().deploy_factory(multisig.address)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test09_it_fails_when_factory_is_not_authorized_by_multisig")

        def test10_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).deploySet(
                    {"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test10_it_fails_when_caller_is_not_an_admin")

        def test11_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test11_it_fails_when_called_twice_by_the_same_admin")

        def test12_it_launches_successfully_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            with self.assertRaises(KeyError):
                factory.storage["contract_sets"]["set1"]()
            self.assertEqual(factory.storage["last_deployed_contracts"](), {
                             "nft": ALICE_PK, "minter": ALICE_PK, "revealer": ALICE_PK})
            bob_pytezos.contract(factory.address).deploySet(
                {"deploy": "set1"}).send(**send_conf)
            self.assertEqual(factory.storage["multisig"](), multisig.address)
            self.assertEqual(factory.storage["doga_address"](), ALICE_PK)
            self.assertEqual(factory.storage["default_reserve"](), ALICE_PK)

            last_deployed_contracts = factory.storage["last_deployed_contracts"](
            )
            self.assertEqual(
                factory.storage["contract_sets"]["set1"](), last_deployed_contracts)

            minter_address = factory.storage["last_deployed_contracts"]["minter"](
            )
            revealer_address = factory.storage["last_deployed_contracts"]["revealer"](
            )
            nft_address = factory.storage["last_deployed_contracts"]["nft"]()
            minter = alice_pytezos.contract(minter_address)
            revealer = alice_pytezos.contract(revealer_address)
            nft = alice_pytezos.contract(nft_address)
            self.assertIn(factory.address,
                          multisig.storage["authorized_contracts"]())
            self.assertEqual(minter.storage["nft_address"](), nft_address)
            self.assertEqual(revealer.storage["nft_address"](), nft_address)
            self.assertIn(revealer_address, nft.storage["contracts"]())
            self.assertIn(minter_address, nft.storage["contracts"]())
            TestFactory.print_success(
                "test12_it_launches_successfully_with_two_admins")

    class UpdateMultisig(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_updates_multisig_address(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            self.assertEqual(factory.storage["multisig"](), multisig.address)
            new_multisig = Env().deploy_multisig()
            factory.updateMultisig(new_multisig.address).send(**send_conf)
            self.assertEqual(
                factory.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                mint_controller.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                reveal_controller.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                nft_controller.storage["multisig"](), new_multisig.address)
            TestFactory.print_success(
                "test01_it_updates_multisig_address")

        def test02_it_updates_multisig_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertEqual(factory.storage["multisig"](), multisig.address)
            self.assertEqual(
                mint_controller.storage["multisig"](), multisig.address)
            self.assertEqual(
                reveal_controller.storage["multisig"](), multisig.address)
            self.assertEqual(
                nft_controller.storage["multisig"](), multisig.address)
            new_multisig = Env().deploy_multisig()
            factory.updateMultisig(new_multisig.address).send(**send_conf)
            self.assertEqual(factory.storage["multisig"](), multisig.address)
            self.assertEqual(
                mint_controller.storage["multisig"](), multisig.address)
            self.assertEqual(
                reveal_controller.storage["multisig"](), multisig.address)
            self.assertEqual(
                nft_controller.storage["multisig"](), multisig.address)
            bob_pytezos.contract(factory.address).updateMultisig(
                new_multisig.address).send(**send_conf)
            self.assertEqual(
                factory.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                mint_controller.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                reveal_controller.storage["multisig"](), new_multisig.address)
            self.assertEqual(
                nft_controller.storage["multisig"](), new_multisig.address)
            TestFactory.print_success(
                "test02_it_updates_multisig_address_with_two_admins")

        # IT DOESN'T FAIL!
        # multisig=ALICE_PK, so when Alice calls, sender <> multisig = false, multisig call is skipped and the storage is updated.
        # TODO: should this behaviour be avoided? this could be a way to have one admin and avoid cross-contract calls,
        # but could cause unwanted behaviour such as failure of the factory's add_multisig_authorized_contract function
        def skip_test03_it_fails_when_multisig_has_no_call_entrypoint(self):
            return ()
            factory = Env().deploy_factory(multisig=ALICE_PK)
            new_multisig = Env().deploy_multisig()
            # with self.assertRaises(MichelsonError) as err:
            factory.updateMultisig(new_multisig.address).send(**send_conf)
            # self.assertEqual(err.exception.args[0]["with"], {
            #                  "string": "no add authorized contract entrypoint"})
            TestFactory.print_success(
                "test03_it_fails_when_multisig_has_no_call_entrypoint")

        def test04_it_fails_when_factory_is_not_authorized_by_multisig(self):
            multisig = Env().deploy_multisig()
            factory = Env().deploy_factory(multisig=multisig.address)
            new_multisig = Env().deploy_multisig()
            with self.assertRaises(MichelsonError) as err:
                factory.updateMultisig(new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test04_it_fails_when_factory_is_not_authorized_by_multisig")

        def test05_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_multisig = Env().deploy_multisig()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).updateMultisig(
                    new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test05_it_fails_when_caller_is_not_an_admin")

        def test06_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            new_multisig = Env().deploy_multisig()
            factory.updateMultisig(new_multisig.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.updateMultisig(new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test06_it_fails_when_called_twice_by_the_same_admin")

    class SetPause(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_pause_successfully_single_minter(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["last_deployed_contracts"]["minter"](
            )
            minter = alice_pytezos.contract(minter_addr)
            self.assertEqual(minter.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"minter": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(minter.storage["paused"](), True)
            TestFactory.print_success(
                "test01_it_sets_pause_successfully_single_minter")

        def test02_it_sets_pause_successfully_two_minters(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr1 = factory.storage["last_deployed_contracts"]["minter"](
            )
            minter1 = alice_pytezos.contract(minter_addr1)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr2 = factory.storage["last_deployed_contracts"]["minter"](
            )
            minter2 = alice_pytezos.contract(minter_addr2)
            self.assertEqual(minter1.storage["paused"](), False)
            self.assertEqual(minter2.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"multiple": ["set1", "set2"]},
                "choose_contract": {"minter": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(minter1.storage["paused"](), True)
            self.assertEqual(minter2.storage["paused"](), True)
            TestFactory.print_success(
                "test02_it_sets_pause_successfully_two_minters")

        def test03_it_sets_pause_successfully_single_revealer(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            revealer_addr = factory.storage["last_deployed_contracts"]["revealer"](
            )
            revealer = alice_pytezos.contract(revealer_addr)
            self.assertEqual(revealer.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"revealer": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(revealer.storage["paused"](), True)
            TestFactory.print_success(
                "test03_it_sets_pause_successfully_single_revealer")

        def test04_it_sets_pause_successfully_two_revealers(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            revealer_addr1 = factory.storage["last_deployed_contracts"]["revealer"](
            )
            revealer1 = alice_pytezos.contract(revealer_addr1)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer_addr2 = factory.storage["last_deployed_contracts"]["revealer"](
            )
            revealer2 = alice_pytezos.contract(revealer_addr2)
            self.assertEqual(revealer1.storage["paused"](), False)
            self.assertEqual(revealer2.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"multiple": ["set1", "set2"]},
                "choose_contract": {"revealer": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(revealer1.storage["paused"](), True)
            self.assertEqual(revealer2.storage["paused"](), True)
            TestFactory.print_success(
                "test04_it_sets_pause_successfully_two_revealers")

        def test05_it_sets_pause_successfully_single_both(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            revealer_addr = factory.storage["last_deployed_contracts"]["revealer"](
            )
            minter_addr = factory.storage["last_deployed_contracts"]["minter"](
            )
            revealer = alice_pytezos.contract(revealer_addr)
            minter = alice_pytezos.contract(minter_addr)
            self.assertEqual(revealer.storage["paused"](), False)
            self.assertEqual(minter.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"both": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(revealer.storage["paused"](), True)
            self.assertEqual(minter.storage["paused"](), True)
            TestFactory.print_success(
                "test05_it_sets_pause_successfully_single_both")

        def test06_it_sets_pause_successfully_two_both(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            revealer_addr1 = factory.storage["last_deployed_contracts"]["revealer"](
            )
            minter_addr1 = factory.storage["last_deployed_contracts"]["minter"](
            )
            revealer1 = alice_pytezos.contract(revealer_addr1)
            minter1 = alice_pytezos.contract(minter_addr1)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer_addr2 = factory.storage["last_deployed_contracts"]["revealer"](
            )
            minter_addr2 = factory.storage["last_deployed_contracts"]["minter"](
            )
            revealer2 = alice_pytezos.contract(revealer_addr2)
            minter2 = alice_pytezos.contract(minter_addr2)
            self.assertEqual(revealer1.storage["paused"](), False)
            self.assertEqual(revealer2.storage["paused"](), False)
            self.assertEqual(minter1.storage["paused"](), False)
            self.assertEqual(minter2.storage["paused"](), False)
            set_pause_params = {
                "update_method": {"multiple": ["set1", "set2"]},
                "choose_contract": {"both": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(revealer1.storage["paused"](), True)
            self.assertEqual(revealer2.storage["paused"](), True)
            self.assertEqual(minter1.storage["paused"](), True)
            self.assertEqual(minter2.storage["paused"](), True)
            TestFactory.print_success(
                "test06_it_sets_pause_successfully_two_both")

        def test07_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"both": None},
                "pause": True,
            }
            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test07_it_fails_when_factory_is_not_authorized_by_multisig")

        def test08_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"both": None},
                "pause": True,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setPause(
                    set_pause_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test08_it_fails_when_caller_is_not_an_admin")

        def test09_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_pause_params = {
                "update_method": {"single": "set1"},
                "choose_contract": {"both": None},
                "pause": True,
            }
            factory.setPause(set_pause_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setPause(set_pause_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test09_it_fails_when_called_twice_by_the_same_admin")

    class SetNftAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_nft_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"](
            )
            revealer_addr = factory.storage["contract_sets"]["set1"]["revealer"](
            )
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"](
            )
            minter = alice_pytezos.contract(minter_addr)
            revealer = alice_pytezos.contract(revealer_addr)

            self.assertEqual(minter.storage["nft_address"](), nft_addr)
            new_nft = Env().deploy_nft(FA2Storage(
                factory=factory.address, controller=nft_controller.address))
            set_nft_address_params = {
                "set_id": "set1",
                "nft_address": new_nft.address,
            }
            factory.setNftAddress(set_nft_address_params).send(**send_conf)
            self.assertEqual(minter.storage["nft_address"](), new_nft.address)
            self.assertEqual(
                revealer.storage["nft_address"](), new_nft.address)
            TestFactory.print_success(
                "test01_it_sets_nft_address_successfully_single_minter")

        def test02_it_sets_nft_address_successfully_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"](
            )
            revealer_addr = factory.storage["contract_sets"]["set1"]["revealer"](
            )
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"](
            )
            minter = alice_pytezos.contract(minter_addr)
            revealer = alice_pytezos.contract(revealer_addr)
            self.assertEqual(minter.storage["nft_address"](), nft_addr)
            self.assertEqual(revealer.storage["nft_address"](), nft_addr)
            new_nft = Env().deploy_nft(FA2Storage(
                controller=nft_controller.address, factory=factory.address))
            set_nft_address_params = {
                "set_id": "set1",
                "nft_address": new_nft.address,
            }
            factory.setNftAddress(set_nft_address_params).send(**send_conf)
            self.assertEqual(minter.storage["nft_address"](), nft_addr)
            self.assertEqual(revealer.storage["nft_address"](), nft_addr)
            bob_pytezos.contract(factory.address).setNftAddress(
                set_nft_address_params).send(**send_conf)
            self.assertEqual(minter.storage["nft_address"](), new_nft.address)
            self.assertEqual(
                revealer.storage["nft_address"](), new_nft.address)
            TestFactory.print_success(
                "test02_it_sets_nft_address_successfully_two_admins")

        def test04_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)
            new_nft = Env().deploy_nft(FA2Storage(
                factory=factory.address, controller=nft_controller.address))
            set_nft_address_params = {
                "set_id": "set1",
                "nft_address": new_nft.address,
            }
            with self.assertRaises(MichelsonError) as err:
                factory.setNftAddress(set_nft_address_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test04_it_fails_when_factory_is_not_authorized_by_multisig")

        def test05_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            new_nft = Env().deploy_nft(FA2Storage(
                factory=factory.address, controller=nft_controller.address))
            set_nft_address_params = {
                "set_id": "set1",
                "nft_address": new_nft.address,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setNftAddress(
                    set_nft_address_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test05_it_fails_when_caller_is_not_an_admin")

        def test06_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            new_nft = Env().deploy_nft(FA2Storage(
                factory=factory.address, controller=nft_controller.address))
            set_nft_address_params = {
                "set_id": "set1",
                "nft_address": new_nft.address,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            factory.setNftAddress(set_nft_address_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setNftAddress(set_nft_address_params).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test06_it_fails_when_called_twice_by_the_same_admin")

    class SetDogaAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_updates_doga_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            self.assertEqual(factory.storage["doga_address"](), ALICE_PK)
            factory.setDogaAddress(BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["doga_address"](), BOB_PK)
            TestFactory.print_success(
                "test01_it_updates_doga_address_successfully")

        def test02_it_updates_doga_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertEqual(factory.storage["doga_address"](), ALICE_PK)

            factory.setDogaAddress(BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["doga_address"](), ALICE_PK)
            bob_pytezos.contract(factory.address).setDogaAddress(
                BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["doga_address"](), BOB_PK)
            TestFactory.print_success(
                "test02_it_updates_doga_address_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            multisig = Env().deploy_multisig()
            factory = Env().deploy_factory(multisig=multisig.address)
            with self.assertRaises(MichelsonError) as err:
                factory.setDogaAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setDogaAddress(
                    BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            factory.setDogaAddress(BOB_PK).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setDogaAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetOracleAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_oracle_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            self.assertEqual(factory.storage["default_oracle"](), ALICE_PK)

            factory.setOracleAddress(
                BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["default_oracle"](), BOB_PK)

            TestFactory.print_success(
                "test01_it_sets_oracle_address_successfully")

        def test02_it_sets_oracle_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            self.assertEqual(factory.storage["default_oracle"](), ALICE_PK)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            factory.setOracleAddress(BOB_PK).send(**send_conf)

            self.assertEqual(factory.storage["default_oracle"](), ALICE_PK)

            bob_pytezos.contract(factory.address).setOracleAddress(
                BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["default_oracle"](), BOB_PK)

            TestFactory.print_success(
                "test02_it_sets_oracle_address_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                factory.setOracleAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setOracleAddress(
                    BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            factory.setOracleAddress(BOB_PK).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setOracleAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetDefaultReserve(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_default_reserve_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            self.assertEqual(factory.storage["default_reserve"](), ALICE_PK)

            factory.setDefaultReserve(
                BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["default_reserve"](), BOB_PK)

            TestFactory.print_success(
                "test01_it_sets_default_reserve_successfully")

        def test02_it_sets_default_reserve_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            self.assertEqual(factory.storage["default_reserve"](), ALICE_PK)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            factory.setDefaultReserve(BOB_PK).send(**send_conf)

            self.assertEqual(factory.storage["default_reserve"](), ALICE_PK)

            bob_pytezos.contract(factory.address).setDefaultReserve(
                BOB_PK).send(**send_conf)
            self.assertEqual(factory.storage["default_reserve"](), BOB_PK)

            TestFactory.print_success(
                "test02_it_sets_default_reserve_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                factory.setDefaultReserve(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setDefaultReserve(
                    BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            factory.setDefaultReserve(BOB_PK).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setDefaultReserve(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetRevealerAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_revealer_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)

            revealer_addr = factory.storage["contract_sets"]["set1"]["revealer"](
            )
            revealer2_addr = factory.storage["contract_sets"]["set2"]["revealer"](
            )
            self.assertNotEqual(revealer_addr, revealer2_addr)

            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": revealer2_addr,
            }

            factory.setRevealerAddress(
                set_revealer_address_param).send(**send_conf)
            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["revealer"](), revealer2_addr)

            TestFactory.print_success(
                "test01_it_sets_revealer_address_successfully")

        def test02_it_sets_revealer_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer_addr = factory.storage["contract_sets"]["set1"]["revealer"](
            )
            revealer2_addr = factory.storage["contract_sets"]["set2"]["revealer"](
            )

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": revealer2_addr,
            }

            factory.setRevealerAddress(
                set_revealer_address_param).send(**send_conf)

            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["revealer"](), revealer_addr)

            bob_pytezos.contract(factory.address).setRevealerAddress(
                set_revealer_address_param).send(**send_conf)
            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["revealer"](), revealer2_addr)

            TestFactory.print_success(
                "test02_it_sets_revealer_address_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer2_addr = factory.storage["contract_sets"]["set2"]["revealer"](
            )

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": revealer2_addr,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.setRevealerAddress(
                    set_revealer_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer2_addr = factory.storage["contract_sets"]["set2"]["revealer"](
            )
            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": revealer2_addr,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setRevealerAddress(
                    set_revealer_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            revealer2_addr = factory.storage["contract_sets"]["set2"]["revealer"](
            )
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": revealer2_addr,
            }

            factory.setRevealerAddress(
                set_revealer_address_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setRevealerAddress(
                    set_revealer_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

        def test06_it_fails_when_revealer_has_no_set_nft_address_entrypoint(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_revealer_address_param = {
                "set_id": "set1",
                "revealer_address": BOB_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.setRevealerAddress(
                    set_revealer_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "no setNftAddress entrypoint for the chosen contract"})

            TestFactory.print_success(
                "test06_it_fails_when_revealer_has_no_set_nft_address_entrypoint")

    class SetMinterAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_sets_minter_address_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)

            minter_addr = factory.storage["contract_sets"]["set1"]["minter"](
            )
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"](
            )
            self.assertNotEqual(minter_addr, minter2_addr)

            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": minter2_addr,
            }

            factory.setMinterAddress(
                set_minter_address_param).send(**send_conf)
            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["minter"](), minter2_addr)

            TestFactory.print_success(
                "test01_it_sets_minter_address_successfully")

        def test02_it_sets_minter_address_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"](
            )
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"](
            )

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": minter2_addr,
            }

            factory.setMinterAddress(
                set_minter_address_param).send(**send_conf)

            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["minter"](), minter_addr)

            bob_pytezos.contract(factory.address).setMinterAddress(
                set_minter_address_param).send(**send_conf)
            self.assertEqual(
                factory.storage["contract_sets"]["set1"]["minter"](), minter2_addr)

            TestFactory.print_success(
                "test02_it_sets_minter_address_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"](
            )

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": minter2_addr,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.setMinterAddress(
                    set_minter_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"](
            )
            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": minter2_addr,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).setMinterAddress(
                    set_minter_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            factory.deploySet({"deploy": "set2"}).send(**send_conf)
            minter2_addr = factory.storage["contract_sets"]["set2"]["minter"](
            )
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": minter2_addr,
            }

            factory.setMinterAddress(
                set_minter_address_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.setMinterAddress(
                    set_minter_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

        def test06_it_fails_when_minter_has_no_set_nft_address_entrypoint(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_minter_address_param = {
                "set_id": "set1",
                "minter_address": BOB_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.setMinterAddress(
                    set_minter_address_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "string": "no setNftAddress entrypoint for the chosen contract"})

            TestFactory.print_success(
                "test06_it_fails_when_minter_has_no_set_nft_address_entrypoint")

    class UpdateLaunchers(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_updates_launchers_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_launcher = Env().deploy_mint_launcher(factory_address=factory.address,
                                                           controller_address=mint_controller.address)
            self.assertNotEqual(
                factory.storage["mint_launcher"](), new_mint_launcher.address)

            new_reveal_launcher = Env().deploy_reveal_launcher(factory_address=factory.address,
                                                               controller_address=reveal_controller.address)

            self.assertNotEqual(
                factory.storage["reveal_launcher"](), new_reveal_launcher.address)

            new_nft_launcher = Env().deploy_nft_launcher(factory_address=factory.address,
                                                         controller_address=nft_controller.address)

            self.assertNotEqual(
                factory.storage["nft_launcher"](), new_nft_launcher.address)

            update_launchers_param = {
                "mint_launcher": new_mint_launcher.address,
                "reveal_launcher": new_reveal_launcher.address,
                "nft_launcher": new_nft_launcher.address,
            }

            factory.updateLaunchers(
                update_launchers_param).send(**send_conf)
            self.assertEqual(
                factory.storage["mint_launcher"](), new_mint_launcher.address)
            self.assertEqual(
                factory.storage["reveal_launcher"](), new_reveal_launcher.address)
            self.assertEqual(
                factory.storage["nft_launcher"](), new_nft_launcher.address)

            TestFactory.print_success(
                "test01_it_updates_launchers_successfully")

        def test02_it_updates_launchers_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_launcher = Env().deploy_mint_launcher(factory_address=factory.address,
                                                           controller_address=mint_controller.address)

            mint_launcher_addr = factory.storage["mint_launcher"]()
            self.assertNotEqual(
                mint_launcher_addr, new_mint_launcher.address)

            new_reveal_launcher = Env().deploy_reveal_launcher(factory_address=factory.address,
                                                               controller_address=reveal_controller.address)

            reveal_launcher_addr = factory.storage["reveal_launcher"]()
            self.assertNotEqual(
                reveal_launcher_addr, new_reveal_launcher.address)

            new_nft_launcher = Env().deploy_nft_launcher(factory_address=factory.address,
                                                         controller_address=nft_controller.address)

            nft_launcher_addr = factory.storage["nft_launcher"]()
            self.assertNotEqual(
                nft_launcher_addr, new_nft_launcher.address)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            update_launchers_param = {
                "mint_launcher": new_mint_launcher.address,
                "reveal_launcher": new_reveal_launcher.address,
                "nft_launcher": new_nft_launcher.address,
            }

            factory.updateLaunchers(
                update_launchers_param).send(**send_conf)

            self.assertEqual(
                factory.storage["mint_launcher"](), mint_launcher_addr)
            self.assertEqual(
                factory.storage["reveal_launcher"](), reveal_launcher_addr)
            self.assertEqual(
                factory.storage["nft_launcher"](), nft_launcher_addr)

            bob_pytezos.contract(factory.address).updateLaunchers(
                update_launchers_param).send(**send_conf)
            self.assertEqual(
                factory.storage["mint_launcher"](), new_mint_launcher.address)
            self.assertEqual(
                factory.storage["reveal_launcher"](), new_reveal_launcher.address)
            self.assertEqual(
                factory.storage["nft_launcher"](), new_nft_launcher.address)

            TestFactory.print_success(
                "test02_it_updates_launchers_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_launcher = Env().deploy_mint_launcher(factory_address=factory.address,
                                                           controller_address=mint_controller.address)
            new_reveal_launcher = Env().deploy_reveal_launcher(factory_address=factory.address,
                                                               controller_address=reveal_controller.address)
            new_nft_launcher = Env().deploy_nft_launcher(factory_address=factory.address,
                                                         controller_address=nft_controller.address)

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            update_launchers_param = {
                "mint_launcher": new_mint_launcher.address,
                "reveal_launcher": new_reveal_launcher.address,
                "nft_launcher": new_nft_launcher.address,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.updateLaunchers(
                    update_launchers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_launcher = Env().deploy_mint_launcher(factory_address=factory.address,
                                                           controller_address=mint_controller.address)
            new_reveal_launcher = Env().deploy_reveal_launcher(factory_address=factory.address,
                                                               controller_address=reveal_controller.address)
            new_nft_launcher = Env().deploy_nft_launcher(factory_address=factory.address,
                                                         controller_address=nft_controller.address)
            update_launchers_param = {
                "mint_launcher": new_mint_launcher.address,
                "reveal_launcher": new_reveal_launcher.address,
                "nft_launcher": new_nft_launcher.address,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).updateLaunchers(
                    update_launchers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_launcher = Env().deploy_mint_launcher(factory_address=factory.address,
                                                           controller_address=mint_controller.address)
            new_reveal_launcher = Env().deploy_reveal_launcher(factory_address=factory.address,
                                                               controller_address=reveal_controller.address)
            new_nft_launcher = Env().deploy_nft_launcher(factory_address=factory.address,
                                                         controller_address=nft_controller.address)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            update_launchers_param = {
                "mint_launcher": new_mint_launcher.address,
                "reveal_launcher": new_reveal_launcher.address,
                "nft_launcher": new_nft_launcher.address,
            }

            factory.updateLaunchers(
                update_launchers_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.updateLaunchers(
                    update_launchers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateControllers(unittest.TestCase):
        def test00_before_tests(self):
            TestFactory.print_title(self)

        def test01_it_updates_controllers_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_controller = Env().deploy_mint_controller(factory=factory.address,
                                                               multisig=multisig.address)
            self.assertEqual(
                factory.storage["mint_controller"](), mint_controller.address)

            new_reveal_controller = Env().deploy_reveal_controller(factory=factory.address,
                                                                   multisig=multisig.address)

            self.assertEqual(
                factory.storage["reveal_controller"](), reveal_controller.address)

            new_nft_controller = Env().deploy_nft_controller(factory=factory.address,
                                                             multisig=multisig.address)

            self.assertEqual(
                factory.storage["nft_controller"](), nft_controller.address)

            update_controllers_param = {
                "mint_controller": new_mint_controller.address,
                "reveal_controller": new_reveal_controller.address,
                "nft_controller": new_nft_controller.address,
            }

            factory.updateControllers(
                update_controllers_param).send(**send_conf)
            self.assertEqual(
                factory.storage["mint_controller"](), new_mint_controller.address)
            self.assertEqual(
                factory.storage["reveal_controller"](), new_reveal_controller.address)
            self.assertEqual(
                factory.storage["nft_controller"](), new_nft_controller.address)

            TestFactory.print_success(
                "test01_it_updates_controllers_successfully")

        def test02_it_updates_controllers_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_controller = Env().deploy_mint_controller(factory=factory.address,
                                                               multisig=multisig.address)

            mint_controller_addr = factory.storage["mint_controller"]()
            self.assertNotEqual(
                mint_controller_addr, new_mint_controller.address)

            new_reveal_controller = Env().deploy_reveal_controller(factory=factory.address,
                                                                   multisig=multisig.address)

            reveal_controller_addr = factory.storage["reveal_controller"]()
            self.assertNotEqual(
                reveal_controller_addr, new_reveal_controller.address)

            new_nft_controller = Env().deploy_nft_controller(factory=factory.address,
                                                             multisig=multisig.address)

            nft_controller_addr = factory.storage["nft_controller"]()
            self.assertNotEqual(
                nft_controller_addr, new_nft_controller.address)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            update_controllers_param = {
                "mint_controller": new_mint_controller.address,
                "reveal_controller": new_reveal_controller.address,
                "nft_controller": new_nft_controller.address,
            }

            factory.updateControllers(
                update_controllers_param).send(**send_conf)

            self.assertEqual(
                factory.storage["mint_controller"](), mint_controller_addr)
            self.assertEqual(
                factory.storage["reveal_controller"](), reveal_controller_addr)
            self.assertEqual(
                factory.storage["nft_controller"](), nft_controller_addr)

            bob_pytezos.contract(factory.address).updateControllers(
                update_controllers_param).send(**send_conf)
            self.assertEqual(
                factory.storage["mint_controller"](), new_mint_controller.address)
            self.assertEqual(
                factory.storage["reveal_controller"](), new_reveal_controller.address)
            self.assertEqual(
                factory.storage["nft_controller"](), new_nft_controller.address)

            TestFactory.print_success(
                "test02_it_updates_controllers_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_controller = Env().deploy_mint_controller(factory=factory.address,
                                                               multisig=multisig.address)
            new_reveal_controller = Env().deploy_reveal_controller(factory=factory.address,
                                                                   multisig=multisig.address)
            new_nft_controller = Env().deploy_nft_controller(factory=factory.address,
                                                             multisig=multisig.address)

            multisig.removeAuthorizedContract(
                factory.address).send(**send_conf)

            update_controllers_param = {
                "mint_controller": new_mint_controller.address,
                "reveal_controller": new_reveal_controller.address,
                "nft_controller": new_nft_controller.address,
            }

            with self.assertRaises(MichelsonError) as err:
                factory.updateControllers(
                    update_controllers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestFactory.print_success(
                "test03_it_fails_when_factory_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_controller = Env().deploy_mint_controller(factory=factory.address,
                                                               multisig=multisig.address)
            new_reveal_controller = Env().deploy_reveal_controller(factory=factory.address,
                                                                   multisig=multisig.address)
            new_nft_controller = Env().deploy_nft_controller(factory=factory.address,
                                                             multisig=multisig.address)
            update_controllers_param = {
                "mint_controller": new_mint_controller.address,
                "reveal_controller": new_reveal_controller.address,
                "nft_controller": new_nft_controller.address,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(factory.address).updateControllers(
                    update_controllers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestFactory.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_mint_controller = Env().deploy_mint_controller(factory=factory.address,
                                                               multisig=multisig.address)
            new_reveal_controller = Env().deploy_reveal_controller(factory=factory.address,
                                                                   multisig=multisig.address)
            new_nft_controller = Env().deploy_nft_controller(factory=factory.address,
                                                             multisig=multisig.address)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            update_controllers_param = {
                "mint_controller": new_mint_controller.address,
                "reveal_controller": new_reveal_controller.address,
                "nft_controller": new_nft_controller.address,
            }

            factory.updateControllers(
                update_controllers_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                factory.updateControllers(
                    update_controllers_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestFactory.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    def test_inner_test_class(self):
        test_classes_to_run = [
            self.LaunchSet,
            self.UpdateMultisig,
            self.SetPause,
            self.SetNftAddress,
            self.SetDogaAddress,
            self.SetOracleAddress,
            self.SetDefaultReserve,
            self.SetRevealerAddress,
            self.SetMinterAddress,
            self.UpdateLaunchers,
            self.UpdateControllers,
        ]
        suites_list = []
        for test_class in test_classes_to_run:
            suites_list.append(
                unittest.TestLoader().loadTestsFromTestCase(test_class))

        big_suite = unittest.TestSuite(suites_list)
        unittest.TextTestRunner().run(big_suite)


if __name__ == "__main__":
    unittest.main()
