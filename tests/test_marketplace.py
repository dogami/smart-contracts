from pytezos.rpc.errors import MichelsonError
from test_env import Env, Keys, MarketplaceStorage, MultisigStorage, FA2Storage, FA12Storage

import unittest

import logging

logging.basicConfig(level=logging.INFO)


ALICE_PK = Keys().ALICE_PK
BOB_PK = Keys().BOB_PK
RESERVE_PK = Keys().RESERVE_PK

alice_pytezos = Keys().alice_pytezos
bob_pytezos = Keys().bob_pytezos
reserve_pytezos = Keys().reserve_pytezos

send_conf = dict(min_confirmations=1)


class TestMarketplace(unittest.TestCase):
    @staticmethod
    def print_title(instance):
        print("Test Factory: " + instance.__class__.__name__ + "...")
        print("-----------------------------------")

    @staticmethod
    def print_success(function_name):
        print(function_name + "... ok")
        print("-----------------------------------")

    class AddToMarketplace(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_adds_to_marketplace_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            # test success
            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            self.assertEqual(
                marketplace.storage["swaps"][swap_id]["owner"](), ALICE_PK)
            self.assertEqual(
                marketplace.storage["swaps"][swap_id]["token_id"](), token_id
            )
            self.assertEqual(
                marketplace.storage["swaps"][swap_id]["token_price"](), price
            )
            self.assertEqual(
                marketplace.storage["swaps"][swap_id]["start_time"](
                ), start_time
            )
            self.assertEqual(
                marketplace.storage["swaps"][swap_id]["end_time"](), end_time
            )
            self.assertEqual(nft.storage["ledger"]
                             [token_id](), marketplace.address)
            self.assertEqual(
                marketplace.storage["next_swap_id"](), swap_id + 1)
            TestMarketplace.print_success(
                "test01_it_adds_to_marketplace_successfully")

        def test02_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})
            TestMarketplace.print_success(
                "test02_it_fails_when_marketplace_is_paused")

        def test03_it_fails_when_token_origin_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            other_nft = Env().deploy_nft(FA2Storage())
            other_nft.mint(
                {"to_": ALICE_PK, "token_id": token_id, "token_info": {}}
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": other_nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            other_nft.update_operators(
                update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "152"})
            TestMarketplace.print_success(
                "test03_it_fails_when_token_origin_is_not_listed"
            )

        def test04_it_fails_when_swap_is_not_multi_token_and_wrong_token_symbol_is_chosen(
            self,
        ):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": False,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "148"})
            TestMarketplace.print_success(
                "test04_it_fails_when_swap_is_not_multi_token_and_wrong_token_symbol_is_chosen"
            )

        def test05_it_fails_when_token_symbol_is_not_XTZ_and_amount_is_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).with_amount(
                    price
                ).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "133"})
            TestMarketplace.print_success(
                "test05_it_fails_when_token_symbol_is_not_XTZ_and_amount_is_sent"
            )

        def test06_it_fails_when_token_is_already_on_sale(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }
            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "118"})
            TestMarketplace.print_success(
                "test06_it_fails_when_token_is_already_on_sale"
            )

        def test07_it_fails_when_start_time_is_later_than_end_time(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": end_time,
                "end_time": start_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }
            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "120"})
            TestMarketplace.print_success(
                "test07_it_fails_when_start_time_is_later_than_end_time"
            )

        def test08_it_fails_when_accepted_tokens_list_is_not_valid(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": ["XTZ", "USD", "ETH"],
                "is_multi_token": True,
            }
            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "134"})
            TestMarketplace.print_success(
                "test08_it_fails_when_accepted_tokens_list_is_not_valid"
            )

        def test09_it_fails_when_token_symbol_is_unlisted(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "ETH",
                "accepted_tokens": [
                    "XTZ",
                    "USD",
                ],
                "is_multi_token": True,
            }
            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "131"})
            TestMarketplace.print_success(
                "test09_it_fails_when_token_symbol_is_unlisted"
            )

        def test10_it_fails_when_dutch_duration_is_longer_than_swap_duration(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {
                    "dutch": {
                        "duration": (end_time - start_time) + 1,
                        "starting_price": price + 100,
                    }
                },
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "USD",
                "accepted_tokens": [
                    "XTZ",
                    "USD",
                ],
                "is_multi_token": True,
            }
            nft.update_operators(update_operator_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addToMarketplace(add_to_marketplace_params).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "127"})
            TestMarketplace.print_success(
                "test10_it_fails_when_dutch_duration_is_longer_than_swap_duration"
            )

    class RemoveFromMarketplace(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_removes_from_marketplace_succesfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            # test success
            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }
            token_key = {
                "token_id": token_id,
                "token_origin": nft.address,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            self.assertIsNotNone(marketplace.storage["swaps"][swap_id]())
            self.assertIsNotNone(marketplace.storage["tokens"][token_key]())
            marketplace.removeFromMarketplace(swap_id).send(**send_conf)
            with self.assertRaises(KeyError):
                marketplace.storage["swaps"][swap_id]()
            with self.assertRaises(KeyError):
                marketplace.storage["tokens"][token_key]()
            TestMarketplace.print_success(
                "test01_it_removes_from_marketplace_succesfully"
            )

        def test02_it_fails_when_swap_id_does_not_exist(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.removeFromMarketplace(
                    swap_id + 1).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "103"})

            TestMarketplace.print_success(
                "test02_it_fails_when_swap_id_does_not_exist")

        def test03_it_fails_when_caller_is_not_owner(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).removeFromMarketplace(
                    swap_id
                ).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "104"})

            TestMarketplace.print_success(
                "test03_it_fails_when_caller_is_not_owner")

    class Collect(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_collects_successfuly(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }
            token_key = {
                "token_id": token_id,
                "token_origin": nft.address,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "XTZ",
                "amount_ft": price,
                "to_": ALICE_PK,
            }
            resp = (
                bob_pytezos.contract(marketplace.address)
                .collect(collect_param)
                .with_amount(price)
                .send(**send_conf)
            )
            internal_operations = resp.opg_result["contents"][0]["metadata"][
                "internal_operation_results"
            ]
            fee = marketplace.storage["management_fee_rate"]() * price // 10000
            royalties = marketplace.storage["royalties_rate"](
            ) * price // 10000
            seller_value = price - (fee + royalties)
            self.assertEqual(
                internal_operations[1]["destination"], marketplace.storage["treasury"](
                )
            )
            self.assertEqual(int(internal_operations[1]["amount"]), fee)

            # royalties
            self.assertEqual(
                internal_operations[2]["destination"],
                marketplace.storage["royalties_address"](),
            )
            self.assertEqual(int(internal_operations[2]["amount"]), royalties)

            # issuer
            self.assertEqual(internal_operations[3]["destination"], ALICE_PK)
            self.assertEqual(
                int(internal_operations[3]["amount"]), seller_value)

            with self.assertRaises(KeyError):
                marketplace.storage["swaps"][swap_id]()
            with self.assertRaises(KeyError):
                marketplace.storage["tokens"][token_key]()
            TestMarketplace.print_success("test01_it_collects_successfuly")

        def test02_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "XTZ",
                "amount_ft": price,
                "to_": ALICE_PK,
            }
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).collect(
                    collect_param
                ).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})
            TestMarketplace.print_success(
                "test02_it_fails_when_marketplace_is_paused")

        def test03_it_fails_when_token_symbol_is_not_xtz_and_amount_is_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "USD",
                "amount_ft": price,
                "to_": ALICE_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).collect(
                    collect_param
                ).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "133"})
            TestMarketplace.print_success(
                "test03_it_fails_when_token_symbol_is_not_xtz_and_amount_is_sent"
            )

        def test04_it_fails_when_swap_is_reserved_and_collect_to_is_not_the_recepient(
            self,
        ):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            start_time = alice_pytezos.now()
            end_time = alice_pytezos.now() + 2
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"reserved": ALICE_PK},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "XTZ",
                "amount_ft": price,
                "to_": BOB_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).collect(
                    collect_param
                ).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "128"})
            TestMarketplace.print_success(
                "test04_it_fails_when_swap_is_reserved_and_collect_to_is_not_the_recepient"
            )

        def test05_it_fails_when_called_before_sale_start(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": alice_pytezos.now() + 10,
                "end_time": alice_pytezos.now() + 20,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "XTZ",
                "amount_ft": price,
                "to_": ALICE_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).collect(
                    collect_param
                ).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "109"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_before_sale_start"
            )

        def test06_it_fails_when_called_after_sale_ends(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)

            swap_id = marketplace.storage["next_swap_id"]()
            price = 10**6
            update_operator_params = [
                {
                    "add_operator": {
                        "owner": ALICE_PK,
                        "operator": marketplace.address,
                        "token_id": token_id,
                    }
                }
            ]
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": alice_pytezos.now() - 20,
                "end_time": alice_pytezos.now() - 10,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            nft.update_operators(update_operator_params).send(**send_conf)
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            collect_param = {
                "swap_id": swap_id,
                "token_symbol": "XTZ",
                "amount_ft": price,
                "to_": ALICE_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).collect(
                    collect_param
                ).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "110"})
            TestMarketplace.print_success(
                "test06_it_fails_when_called_after_sale_ends")

    class SendOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_sends_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }

            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            TestMarketplace.print_success("test01_it_sends_offer_successfully")

        def test02_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(
                    price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})

            TestMarketplace.print_success(
                "test02_it_fails_when_marketplace_is_paused")

        def test03_it_fails_when_token_origin_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            other_token = Env().deploy_nft(FA2Storage())
            other_token.mint(
                {
                    "to_": ALICE_PK,
                    "token_id": token_id,
                    "token_info": {},
                }
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": other_token.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(
                    price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "152"})

            TestMarketplace.print_success(
                "test03_it_fails_when_token_origin_is_not_listed"
            )

        def test04_it_fails_when_start_time_is_later_than_end_time(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() + 110
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(
                    price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "120"})

            TestMarketplace.print_success(
                "test04_it_fails_when_start_time_is_later_than_end_time"
            )

        def test05_it_fails_when_token_symbol_is_not_xtz_and_amount_is_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "USD",
                "ft_amount": price,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(
                    price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "133"})

            TestMarketplace.print_success(
                "test05_it_fails_when_token_symbol_is_not_xtz_and_amount_is_sent"
            )

        def test06_it_fails_when_offer_is_placed_twice(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(
                    price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "115"})

            TestMarketplace.print_success(
                "test06_it_fails_when_offer_is_placed_twice")

        def test07_it_fails_when_offer_value_is_not_equal_to_amount_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).with_amount(price + 1).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "140"})

            TestMarketplace.print_success(
                "test07_it_fails_when_offer_value_is_not_equal_to_amount_sent"
            )

        def test08_it_fails_when_token_symbol_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "ETH",
                "ft_amount": price,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.sendOffer(offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "131"})

            TestMarketplace.print_success(
                "test08_it_fails_when_token_symbol_is_not_listed"
            )

    class UpdateOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "DOGA"
            marketplace.updateOffer(offer_param).send(**send_conf)
            offer_info = {
                "value": price + 1,
                "start_time": start_time + 1,
                "end_time": end_time + 1,
                "token_symbol": "DOGA",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            TestMarketplace.print_success(
                "test01_it_updates_offer_successfully")

        def test02_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "DOGA"
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})
            TestMarketplace.print_success(
                "test02_it_fails_when_marketplace_is_paused")

        def test03_it_fails_when_token_symbol_is_not_xtz_but_amount_is_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "DOGA"
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).with_amount(price + 1).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "133"})
            TestMarketplace.print_success(
                "test03_it_fails_when_token_symbol_is_not_xtz_but_amount_is_sent"
            )

        def test04_it_fails_when_start_time_is_later_than_end_time(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = end_time + 1
            offer_param["end_time"] = start_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "DOGA"
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "120"})
            TestMarketplace.print_success(
                "test04_it_fails_when_start_time_is_later_than_end_time"
            )

        def test05_it_fails_when_token_symbol_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "ETH"
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "131"})
            TestMarketplace.print_success(
                "test05_it_fails_when_token_symbol_is_not_listed"
            )

        def test06_it_fails_when_offer_does_not_exist(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "DOGA"
            offer_param["token_id"] = token_id + 1
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "116"})
            TestMarketplace.print_success(
                "test06_it_fails_when_offer_does_not_exist")

        def test07_it_fails_when_amount_sent_not_equal_to_ft_amount(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)
            doga_address = marketplace.storage["allowed_tokens"]["DOGA"]["fa_address"](
            )
            doga = alice_pytezos.contract(doga_address)
            doga.approve({"spender": marketplace.address, "value": price + 1}).send(
                **send_conf
            )
            offer_param["start_time"] = start_time + 1
            offer_param["end_time"] = end_time + 1
            offer_param["ft_amount"] = price + 1
            offer_param["token_symbol"] = "XTZ"
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOffer(offer_param).with_amount(price).send(
                    **send_conf
                )
            self.assertEqual(err.exception.args[0]["with"], {"int": "140"})
            TestMarketplace.print_success(
                "test07_it_fails_when_amount_sent_not_equal_to_ft_amount"
            )

    class WithdrawOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_withdraws_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            marketplace.withdrawOffer(
                {"token_id": token_id, "token_origin": nft.address}
            ).send(**send_conf)

            with self.assertRaises(KeyError):
                marketplace.storage["offers"][offer_id]()

            TestMarketplace.print_success(
                "test01_it_withdraws_offer_successfully")

        def test02_it_fails_when_offer_does_not_exist(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            with self.assertRaises(MichelsonError) as err:
                marketplace.withdrawOffer(
                    {"token_id": token_id + 1, "token_origin": nft.address}
                ).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "116"})

            TestMarketplace.print_success(
                "test02_it_fails_when_offer_does_not_exist")

    class AcceptOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_accepts_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            resp = marketplace.acceptOffer(
                accept_offer_param).send(**send_conf)
            with self.assertRaises(KeyError):
                marketplace.storage["offers"][offer_id]()
            token_key = {
                "token_id": token_id,
                "token_origin": nft.address,
            }
            with self.assertRaises(KeyError):
                marketplace.storage["tokens"][token_key]()

            internal_operations = resp.opg_result["contents"][0]["metadata"][
                "internal_operation_results"
            ]
            fee = marketplace.storage["management_fee_rate"]() * price // 10000
            royalties = marketplace.storage["royalties_rate"](
            ) * price // 10000
            seller_value = price - (fee + royalties)
            self.assertEqual(
                internal_operations[0]["destination"], marketplace.storage["treasury"](
                )
            )
            self.assertEqual(int(internal_operations[0]["amount"]), fee)

            self.assertEqual(
                internal_operations[1]["destination"], marketplace.storage["royalties_address"](
                )
            )
            self.assertEqual(int(internal_operations[1]["amount"]), royalties)

            self.assertEqual(
                internal_operations[2]["destination"], ALICE_PK
            )
            self.assertEqual(
                int(internal_operations[2]["amount"]), seller_value)

            self.assertEqual(
                internal_operations[3]["destination"], nft.address
            )

            TestMarketplace.print_success(
                "test01_it_accepts_offer_successfully")

        def test02_it_accepts_offer_successfully_with_swap(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }
            swap_id = marketplace.storage["next_swap_id"]()
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)
            self.assertIsNotNone(marketplace.storage["swaps"][swap_id]())

            resp = marketplace.acceptOffer(
                accept_offer_param).send(**send_conf)

            with self.assertRaises(KeyError):
                marketplace.storage["offers"][offer_id]()
            token_key = {
                "token_id": token_id,
                "token_origin": nft.address,
            }
            with self.assertRaises(KeyError):
                marketplace.storage["tokens"][token_key]()

            with self.assertRaises(KeyError):
                marketplace.storage["swaps"][swap_id]()

            internal_operations = resp.opg_result["contents"][0]["metadata"][
                "internal_operation_results"
            ]
            fee = marketplace.storage["management_fee_rate"]() * price // 10000
            royalties = marketplace.storage["royalties_rate"](
            ) * price // 10000
            seller_value = price - (fee + royalties)
            self.assertEqual(
                internal_operations[0]["destination"], marketplace.storage["treasury"](
                )
            )
            self.assertEqual(int(internal_operations[0]["amount"]), fee)

            self.assertEqual(
                internal_operations[1]["destination"], marketplace.storage["royalties_address"](
                )
            )
            self.assertEqual(int(internal_operations[1]["amount"]), royalties)

            self.assertEqual(
                internal_operations[2]["destination"], ALICE_PK
            )
            self.assertEqual(
                int(internal_operations[2]["amount"]), seller_value)

            self.assertEqual(
                internal_operations[3]["destination"], nft.address
            )

            TestMarketplace.print_success(
                "test02_it_accepts_offer_successfully_with_swap")

        def test03_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.acceptOffer(accept_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})

            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_paused")

        def test04_it_fails_when_offer_does_not_exist(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)
            offer_id = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            offer_info = {
                "value": price,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
            }
            self.assertEqual(
                marketplace.storage["offers"][offer_id](), offer_info)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id + 1,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.acceptOffer(accept_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "113"})

            TestMarketplace.print_success(
                "test04_it_fails_when_offer_does_not_exist")

        def test05_it_fails_when_too_early(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time + 100,
                "end_time": end_time + 120,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.acceptOffer(accept_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "111"})

            TestMarketplace.print_success(
                "test05_it_fails_when_too_early")

        def test06_it_fails_when_too_late(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now()
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time - 100,
                "end_time": end_time - 80,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            marketplace.sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            accept_offer_param = {
                "buyer": ALICE_PK,
                "token_id": token_id,
                "token_origin": nft.address,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.acceptOffer(accept_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "112"})

            TestMarketplace.print_success(
                "test06_it_fails_when_too_late")

    class MakeCounterOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_makes_counter_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            counter_offer_id = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "seller": ALICE_PK,
            }

            counter_offer_info = {
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
                "seller": ALICE_PK,
            }

            self.assertEqual(
                marketplace.storage["counter_offers"][counter_offer_id](), counter_offer_info)

            TestMarketplace.print_success(
                "test01_it_makes_counter_offer_successfully")

        def test02_it_makes_counter_offer_successfully_with_swap(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            swap_id = marketplace.storage["next_swap_id"]()
            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)

            token_key = {
                "token_id": token_id,
                "token_origin": nft.address,
            }
            self.assertIsNotNone(marketplace.storage["tokens"][token_key]())
            self.assertIsNotNone(marketplace.storage["swaps"][swap_id]())

            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            counter_offer_id = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "seller": ALICE_PK,
            }

            counter_offer_info = {
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
                "seller": ALICE_PK,
            }

            self.assertEqual(
                marketplace.storage["counter_offers"][counter_offer_id](), counter_offer_info)

            with self.assertRaises(KeyError):
                marketplace.storage["tokens"][token_key]()
            with self.assertRaises(KeyError):
                marketplace.storage["swaps"][swap_id]()

            TestMarketplace.print_success(
                "test02_it_makes_counter_offer_successfully_with_swap")

        def test03_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.setPause(True).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                marketplace.makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})

            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_paused")

        def test04_it_fails_when_origin_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            other_nft = Env().deploy_nft(init_storage=FA2Storage())

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": other_nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                marketplace.makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "152"})

            TestMarketplace.print_success(
                "test04_it_fails_when_origin_is_not_listed")

        def test05_it_fails_when_start_time_is_later_than_end_time(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": end_time,
                "end_time": start_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                marketplace.makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "120"})

            TestMarketplace.print_success(
                "test05_it_fails_when_start_time_is_later_than_end_time")

        def test06_it_fails_when_token_symbol_is_not_listed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "ETH",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                marketplace.makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "131"})

            TestMarketplace.print_success(
                "test06_it_fails_when_token_symbol_is_not_listed")

        def test07_it_fails_when_counter_offer_exists(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(
                counter_offer_param).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                marketplace.makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "147"})

            TestMarketplace.print_success(
                "test07_it_fails_when_counter_offer_exists")

        def test08_it_fails_when_sender_does_not_own_the_token(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            add_to_marketplace_params = {
                "swap_type": {"regular": None},
                "token_id": token_id,
                "token_origin": nft.address,
                "token_price": price,
                "start_time": start_time,
                "end_time": end_time,
                "recipient": {"general": None},
                "token_symbol": "XTZ",
                "accepted_tokens": ["XTZ", "USD"],
                "is_multi_token": True,
            }

            marketplace.addToMarketplace(
                add_to_marketplace_params).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).makeCounterOffer(
                    counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "150"})

            TestMarketplace.print_success(
                "test08_it_fails_when_sender_does_not_own_the_token")

    class AcceptCounterOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_accepts_counter_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            resp = bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                accept_counter_offer_param).with_amount(price).send(**send_conf)

            internal_operations = resp.opg_result["contents"][0]["metadata"][
                "internal_operation_results"
            ]
            fee = marketplace.storage["management_fee_rate"]() * price // 10000
            royalties = marketplace.storage["royalties_rate"](
            ) * price // 10000
            seller_value = price - (fee + royalties)

            # management fee
            self.assertEqual(
                internal_operations[0]["destination"], marketplace.storage["treasury"](
                )
            )
            self.assertEqual(int(internal_operations[0]["amount"]), fee)

            # royalties
            self.assertEqual(
                internal_operations[1]["destination"],
                marketplace.storage["royalties_address"](),
            )
            self.assertEqual(int(internal_operations[1]["amount"]), royalties)

            # seller
            self.assertEqual(internal_operations[2]["destination"], ALICE_PK)
            self.assertEqual(
                int(internal_operations[2]["amount"]), seller_value)

            # buyer
            self.assertEqual(internal_operations[3]["destination"], BOB_PK)
            self.assertEqual(
                int(internal_operations[3]["amount"]), price)

            TestMarketplace.print_success(
                "test01_it_accepts_counter_offer_successfully")

        def test02_it_fails_when_marketplace_is_paused(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }
            marketplace.setPause(True).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "117"})

            TestMarketplace.print_success(
                "test02_it_fails_when_marketplace_is_paused")

        def test03_it_fails_when_no_offer_placed(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }
            bob_pytezos.contract(marketplace.address).withdrawOffer(
                {"token_id": token_id, "token_origin": nft.address}).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "116"})

            TestMarketplace.print_success(
                "test03_it_fails_when_no_offer_placed")

        def test04_it_fails_when_no_counter_offer_was_made(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "141"})

            TestMarketplace.print_success(
                "test04_it_fails_when_no_counter_offer_was_made")

        def test05_it_fails_when_too_early(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time + 100,
                "end_time": end_time + 120,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "143"})

            TestMarketplace.print_success(
                "test05_it_fails_when_too_early")

        def test06_it_fails_when_token_symbol_is_not_xtz_and_amount_was_sent(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "USD",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "133"})

            TestMarketplace.print_success(
                "test06_it_fails_when_token_symbol_is_not_xtz_and_amount_was_sent")

        def test_07_it_fails_when_too_late(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time - 1000,
                "end_time": end_time - 990,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "144"})

            TestMarketplace.print_success(
                "test_07_it_fails_when_too_late")

        def test08_it_fails_when_amount_is_not_equal_to_counter_offer_value(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            accept_counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "seller": ALICE_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).acceptCounterOffer(
                    accept_counter_offer_param).with_amount(price + 1).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "114"})

            TestMarketplace.print_success(
                "test08_it_fails_when_amount_is_not_equal_to_counter_offer_value")

    class WithdrawCounterOffer(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_withdraws_counter_offer_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            counter_offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            nft.update_operators(
                [
                    {
                        "add_operator": {
                            "owner": ALICE_PK,
                            "operator": marketplace.address,
                            "token_id": token_id,
                        }
                    }
                ]
            ).send(**send_conf)
            marketplace.makeCounterOffer(counter_offer_param).send(**send_conf)

            counter_offer_id = {
                "token_id": token_id,
                "token_origin": nft.address,
                "buyer": BOB_PK,
                "seller": ALICE_PK,
            }

            counter_offer_info = {
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
                "seller": ALICE_PK,
            }

            self.assertEqual(
                marketplace.storage["counter_offers"][counter_offer_id](), counter_offer_info)

            withdraw_counter_offer_param = {
                "token_id": token_id,
                "buyer": BOB_PK,
                "token_origin": nft.address,
            }
            marketplace.withdrawCounterOffer(
                withdraw_counter_offer_param).send(**send_conf)
            with self.assertRaises(KeyError):
                marketplace.storage["counter_offers"][counter_offer_id]()

            TestMarketplace.print_success(
                "test01_it_withdraws_counter_offer_successfully")

        def test02_it_fails_when_no_counter_offer_was_made(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            minter_addr = factory.storage["contract_sets"]["set1"]["minter"]()
            minter = alice_pytezos.contract(minter_addr)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            marketplace.addCollection(nft.address).send(**send_conf)

            mint_controller.setMintParams(
                {
                    "set_mint_params": {
                        "is_private_sale": False,
                        "mint_price": 1,
                        "currency": "XTZ",
                        "next_token_id": 1,
                        "total_supply": 100,
                        "max_mint_per_address": 100,
                        "token_per_pack": 3,
                        "start_time": alice_pytezos.now() - 15,
                        "end_time": alice_pytezos.now() + 100,
                    },
                    "set_id": "set1",
                }
            ).send(**send_conf)
            token_id = minter.storage["next_token_id"]()
            minter.mintFromCrowdsale({"token_count": 3, "to_": ALICE_PK}).with_amount(
                1
            ).send(**send_conf)
            start_time = alice_pytezos.now() - 10
            end_time = alice_pytezos.now() + 100
            price = 10**6
            offer_param = {
                "token_id": token_id,
                "token_origin": nft.address,
                "start_time": start_time,
                "end_time": end_time,
                "token_symbol": "XTZ",
                "ft_amount": price,
            }
            bob_pytezos.contract(marketplace.address).sendOffer(offer_param).with_amount(
                price).send(**send_conf)

            withdraw_counter_offer_param = {
                "token_id": token_id,
                "buyer": BOB_PK,
                "token_origin": nft.address,
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.withdrawCounterOffer(
                    withdraw_counter_offer_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "141"})

            TestMarketplace.print_success(
                "test02_it_fails_when_no_counter_offer_was_made")

    class SetPause(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_sets_pause_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(marketplace.storage["paused"](), False)
            marketplace.setPause(True).send(**send_conf)
            self.assertEqual(marketplace.storage["paused"](), True)
            TestMarketplace.print_success(
                "test01_it_sets_pause_successfully")

        def test02_it_sets_pause_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(marketplace.storage["paused"](), False)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.setPause(True).send(**send_conf)
            self.assertEqual(marketplace.storage["paused"](), False)
            bob_pytezos.contract(marketplace.address).setPause(
                True).send(**send_conf)
            self.assertEqual(marketplace.storage["paused"](), True)
            TestMarketplace.print_success(
                "test02_it_sets_pause_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.setPause(True).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(marketplace.storage["paused"](), False)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).setPause(
                    True).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(marketplace.storage["paused"](), False)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.setPause(True).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.setPause(True).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class AddCollection(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_add_collection_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            self.assertNotIn(collection_address,
                             marketplace.storage["collections"]())
            marketplace.addCollection(collection_address).send(**send_conf)
            self.assertIn(collection_address,
                          marketplace.storage["collections"]())
            TestMarketplace.print_success(
                "test01_it_adds_collection_successfully")

        def test02_it_adds_collection_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertNotIn(collection_address,
                             marketplace.storage["collections"]())
            marketplace.addCollection(collection_address).send(**send_conf)
            bob_pytezos.contract(marketplace.address).addCollection(
                collection_address).send(**send_conf)
            self.assertIn(collection_address,
                          marketplace.storage["collections"]())
            TestMarketplace.print_success(
                "test02_it_adds_collection_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            with self.assertRaises(MichelsonError) as err:
                marketplace.addCollection(collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).addCollection(
                    collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.addCollection(collection_address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.addCollection(collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class RemoveCollection(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_removes_collection_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            self.assertNotIn(collection_address,
                             marketplace.storage["collections"]())
            marketplace.addCollection(collection_address).send(**send_conf)
            self.assertIn(collection_address,
                          marketplace.storage["collections"]())
            marketplace.removeCollection(collection_address).send(**send_conf)
            self.assertNotIn(collection_address,
                             marketplace.storage["collections"]())
            TestMarketplace.print_success(
                "test01_it_removes_collection_successfully")

        def test02_it_removes_collection_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            marketplace.addCollection(collection_address).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            self.assertIn(collection_address,
                          marketplace.storage["collections"]())
            marketplace.removeCollection(collection_address).send(**send_conf)
            bob_pytezos.contract(marketplace.address).removeCollection(
                collection_address).send(**send_conf)
            self.assertNotIn(collection_address,
                             marketplace.storage["collections"]())
            TestMarketplace.print_success(
                "test02_it_removes_collection_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            marketplace.addCollection(collection_address).send(**send_conf)
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.removeCollection(
                    collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            marketplace.addCollection(collection_address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).removeCollection(
                    collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            collection_address = factory.storage["contract_sets"]["set1"]["nft"](
            )
            marketplace.addCollection(collection_address).send(**send_conf)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.removeCollection(collection_address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.removeCollection(
                    collection_address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateRoyaltiesAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_royalties_address_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(ALICE_PK,
                             marketplace.storage["royalties_address"]())
            marketplace.updateRoyaltiesAddress(BOB_PK).send(**send_conf)
            self.assertEqual(BOB_PK,
                             marketplace.storage["royalties_address"]())
            TestMarketplace.print_success(
                "test01_it_updates_royalties_address_successfully")

        def test02_it_updates_royalties_address_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(ALICE_PK,
                             marketplace.storage["royalties_address"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateRoyaltiesAddress(BOB_PK).send(**send_conf)
            self.assertEqual(ALICE_PK,
                             marketplace.storage["royalties_address"]())
            bob_pytezos.contract(marketplace.address).updateRoyaltiesAddress(
                BOB_PK).send(**send_conf)
            self.assertEqual(BOB_PK,
                             marketplace.storage["royalties_address"]())
            TestMarketplace.print_success(
                "test02_it_updates_royalties_address_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateRoyaltiesAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateRoyaltiesAddress(
                    BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateRoyaltiesAddress(BOB_PK).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateRoyaltiesAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateTreasuryAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_treasury_address_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(RESERVE_PK,
                             marketplace.storage["treasury"]())
            marketplace.updateTreasuryAddress(BOB_PK).send(**send_conf)
            self.assertEqual(BOB_PK,
                             marketplace.storage["treasury"]())
            TestMarketplace.print_success(
                "test01_it_updates_treasury_address_successfully")

        def test02_it_updates_treasury_address_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(RESERVE_PK,
                             marketplace.storage["treasury"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateTreasuryAddress(BOB_PK).send(**send_conf)
            self.assertEqual(RESERVE_PK,
                             marketplace.storage["treasury"]())
            bob_pytezos.contract(marketplace.address).updateTreasuryAddress(
                BOB_PK).send(**send_conf)
            self.assertEqual(BOB_PK,
                             marketplace.storage["treasury"]())
            TestMarketplace.print_success(
                "test02_it_updates_treasury_address_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateTreasuryAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateTreasuryAddress(
                    BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateTreasuryAddress(BOB_PK).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateTreasuryAddress(BOB_PK).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateFee(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_fee_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(250,
                             marketplace.storage["management_fee_rate"]())
            marketplace.updateFee(300).send(**send_conf)
            self.assertEqual(300,
                             marketplace.storage["management_fee_rate"]())
            TestMarketplace.print_success(
                "test01_it_updates_fee_successfully")

        def test02_it_updates_fee_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(250,
                             marketplace.storage["management_fee_rate"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateFee(300).send(**send_conf)
            self.assertEqual(250,
                             marketplace.storage["management_fee_rate"]())
            bob_pytezos.contract(marketplace.address).updateFee(
                300).send(**send_conf)
            self.assertEqual(300,
                             marketplace.storage["management_fee_rate"]())
            TestMarketplace.print_success(
                "test02_it_updates_fee_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateFee(300).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateFee(
                    300).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateFee(300).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateFee(300).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateOracleAddress(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_oracle_address_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_oracle = Env().deploy_normalizer()
            self.assertEqual(oracle.address,
                             marketplace.storage["oracle"]())
            marketplace.updateOracleAddress(
                new_oracle.address).send(**send_conf)
            self.assertEqual(new_oracle.address,
                             marketplace.storage["oracle"]())
            TestMarketplace.print_success(
                "test01_it_updates_oracle_address_successfully")

        def test02_it_updates_oracle_address_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_oracle = Env().deploy_normalizer()
            self.assertEqual(oracle.address,
                             marketplace.storage["oracle"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateOracleAddress(
                new_oracle.address).send(**send_conf)
            self.assertEqual(oracle.address,
                             marketplace.storage["oracle"]())
            bob_pytezos.contract(marketplace.address).updateOracleAddress(
                new_oracle.address).send(**send_conf)
            self.assertEqual(new_oracle.address,
                             marketplace.storage["oracle"]())
            TestMarketplace.print_success(
                "test02_it_updates_oracle_address_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_oracle = Env().deploy_normalizer()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOracleAddress(
                    new_oracle.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_oracle = Env().deploy_normalizer()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateOracleAddress(
                    new_oracle.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_oracle = Env().deploy_normalizer()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateOracleAddress(
                new_oracle.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateOracleAddress(
                    new_oracle.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateRoyalties(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_royalties_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(200,
                             marketplace.storage["royalties_rate"]())
            marketplace.updateRoyalties(
                250).send(**send_conf)
            self.assertEqual(250,
                             marketplace.storage["royalties_rate"]())
            TestMarketplace.print_success(
                "test01_it_updates_royalties_successfully")

        def test02_it_updates_royalties_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(200,
                             marketplace.storage["royalties_rate"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateRoyalties(
                250).send(**send_conf)
            self.assertEqual(200,
                             marketplace.storage["royalties_rate"]())
            bob_pytezos.contract(marketplace.address).updateRoyalties(
                250).send(**send_conf)
            self.assertEqual(250,
                             marketplace.storage["royalties_rate"]())
            TestMarketplace.print_success(
                "test02_it_updates_royalties_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateRoyalties(
                    250).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateRoyalties(
                    250).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateRoyalties(
                250).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateRoyalties(
                    250).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateAllowedTokens(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_allowed_tokens_add_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            with self.assertRaises(KeyError):
                marketplace.storage["allowed_tokens"]["ETH"]()
            eth_contract = Env().deploy_fa12(FA12Storage())
            update_allowed_tokens_param = {
                "token_symbol": "ETH",
                "direction": {
                    "add_token": {
                        "fa_address": eth_contract.address,
                        "fa_type": "fa1.2",
                    }}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["ETH"]())
            self.assertEqual(marketplace.storage["available_pairs"][(
                "ETH", "USD")](), "ETH-USD")
            TestMarketplace.print_success(
                "test01_it_updates_allowed_tokens_add_successfully")

        def test02_it_updates_allowed_tokens_remove_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            update_allowed_tokens_param = {
                "token_symbol": "DOGA",
                "direction": {
                    "remove_token": None}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            with self.assertRaises(KeyError):
                marketplace.storage["allowed_tokens"]["DOGA"]()
            with self.assertRaises(KeyError):
                marketplace.storage["available_pairs"][("DOGA", "USD")]()
            TestMarketplace.print_success(
                "test02_it_updates_allowed_tokens_remove_successfully")

        def test03_it_updates_allowed_tokens_successfully_add_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            with self.assertRaises(KeyError):
                marketplace.storage["allowed_tokens"]["ETH"]()
            eth_contract = Env().deploy_fa12(FA12Storage())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            update_allowed_tokens_param = {
                "token_symbol": "ETH",
                "direction": {
                    "add_token": {
                        "fa_address": eth_contract.address,
                        "fa_type": "fa1.2",
                    }}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            with self.assertRaises(KeyError):
                marketplace.storage["allowed_tokens"]["ETH"]()
            bob_pytezos.contract(marketplace.address).updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["ETH"]())
            self.assertEqual(marketplace.storage["available_pairs"][(
                "ETH", "USD")](), "ETH-USD")
            TestMarketplace.print_success(
                "test03_it_updates_allowed_tokens_successfully_add_with_two_admins")

        def test04_it_updates_allowed_tokens_remove_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            update_allowed_tokens_param = {
                "token_symbol": "DOGA",
                "direction": {
                    "remove_token": None}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["XTZ"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["USD"]())
            self.assertIsNotNone(
                marketplace.storage["allowed_tokens"]["DOGA"]())
            bob_pytezos.contract(marketplace.address).updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            with self.assertRaises(KeyError):
                marketplace.storage["allowed_tokens"]["DOGA"]()
            with self.assertRaises(KeyError):
                marketplace.storage["available_pairs"][("DOGA", "USD")]()
            TestMarketplace.print_success(
                "test04_it_updates_allowed_tokens_remove_successfully_with_two_admins")

        def test05_it_fails_when_marketplace_is_not_authorized_by_multisig_add(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            eth_contract = Env().deploy_fa12(FA12Storage())
            update_allowed_tokens_param = {
                "token_symbol": "ETH",
                "direction": {
                    "add_token": {
                        "fa_address": eth_contract.address,
                        "fa_type": "fa1.2",
                    }}
            }

            with self.assertRaises(MichelsonError) as err:
                marketplace.updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test05_it_fails_when_marketplace_is_not_authorized_by_multisig_add")

        def test06_it_fails_when_marketplace_is_not_authorized_by_multisig_remove(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            update_allowed_tokens_param = {
                "token_symbol": "DOGA",
                "direction": {
                    "remove_token": None}
            }
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test06_it_fails_when_marketplace_is_not_authorized_by_multisig_remove")

        def test07_it_fails_when_caller_is_not_an_admin_add(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            eth_contract = Env().deploy_fa12(FA12Storage())
            update_allowed_tokens_param = {
                "token_symbol": "ETH",
                "direction": {
                    "add_token": {
                        "fa_address": eth_contract.address,
                        "fa_type": "fa1.2",
                    }}
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test07_it_fails_when_caller_is_not_an_admin_add")

        def test08_it_fails_when_caller_is_not_an_admin_remove(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            update_allowed_tokens_param = {
                "token_symbol": "DOGA",
                "direction": {
                    "remove_token": None}
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test08_it_fails_when_caller_is_not_an_admin_remove")

        def test09_it_fails_when_called_twice_by_the_same_admin_add(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            eth_contract = Env().deploy_fa12(FA12Storage())
            update_allowed_tokens_param = {
                "token_symbol": "ETH",
                "direction": {
                    "add_token": {
                        "fa_address": eth_contract.address,
                        "fa_type": "fa1.2",
                    }}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test09_it_fails_when_called_twice_by_the_same_admin_add")

        def test10_it_fails_when_called_twice_by_the_same_admin_remove(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            update_allowed_tokens_param = {
                "token_symbol": "DOGA",
                "direction": {
                    "remove_token": None}
            }
            marketplace.updateAllowedTokens(
                update_allowed_tokens_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateAllowedTokens(
                    update_allowed_tokens_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test10_it_fails_when_called_twice_by_the_same_admin_remove")

    class SetOracleTolerance(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_sets_oracle_tolerance_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(900,
                             marketplace.storage["oracle_tolerance"]())
            marketplace.setOracleTolerance(
                850).send(**send_conf)
            self.assertEqual(850,
                             marketplace.storage["oracle_tolerance"]())
            TestMarketplace.print_success(
                "test01_it_sets_oracle_tolerance_successfully")

        def test02_it_sets_oracle_tolerance_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            self.assertEqual(900,
                             marketplace.storage["oracle_tolerance"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.setOracleTolerance(
                850).send(**send_conf)
            self.assertEqual(900,
                             marketplace.storage["oracle_tolerance"]())
            bob_pytezos.contract(marketplace.address).setOracleTolerance(
                850).send(**send_conf)
            self.assertEqual(850,
                             marketplace.storage["oracle_tolerance"]())
            TestMarketplace.print_success(
                "test02_it_sets_oracle_tolerance_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.setOracleTolerance(
                    850).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).setOracleTolerance(
                    850).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.setOracleTolerance(
                850).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.setOracleTolerance(
                    850).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateMultisig(unittest.TestCase):
        def test00_before_tests(self):
            TestMarketplace.print_title(self)

        def test01_it_updates_multisig_address_successfully(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_multisig = Env().deploy_multisig()
            self.assertEqual(multisig.address,
                             marketplace.storage["multisig"]())
            marketplace.updateMultisig(
                new_multisig.address).send(**send_conf)
            self.assertEqual(new_multisig.address,
                             marketplace.storage["multisig"]())
            TestMarketplace.print_success(
                "test01_it_updates_multisig_address_successfully")

        def test02_it_updates_multisig_address_successfully_with_two_admins(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_multisig = Env().deploy_multisig()
            self.assertEqual(multisig.address,
                             marketplace.storage["multisig"]())
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateMultisig(
                new_multisig.address).send(**send_conf)
            self.assertEqual(multisig.address,
                             marketplace.storage["multisig"]())
            bob_pytezos.contract(marketplace.address).updateMultisig(
                new_multisig.address).send(**send_conf)
            self.assertEqual(new_multisig.address,
                             marketplace.storage["multisig"]())
            TestMarketplace.print_success(
                "test02_it_updates_multisig_address_successfully_with_two_admins")

        def test03_it_fails_when_marketplace_is_not_authorized_by_multisig(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_multisig = Env().deploy_multisig()
            multisig.removeAuthorizedContract(
                marketplace.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateMultisig(
                    new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestMarketplace.print_success(
                "test03_it_fails_when_marketplace_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_multisig = Env().deploy_multisig()
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(marketplace.address).updateMultisig(
                    new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1001"})
            TestMarketplace.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            marketplace, factory, multisig, oracle, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_marketplace_app()
            new_multisig = Env().deploy_multisig()
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            marketplace.updateMultisig(
                new_multisig.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                marketplace.updateMultisig(
                    new_multisig.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {"int": "1004"})
            TestMarketplace.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    def test_inner_test_class(self):
        test_classes_to_run = [
            self.AddToMarketplace,
            self.RemoveFromMarketplace,
            self.Collect,
            self.SendOffer,
            self.UpdateOffer,
            self.WithdrawOffer,
            self.AcceptOffer,
            self.MakeCounterOffer,
            self.AcceptCounterOffer,
            self.WithdrawCounterOffer,
            self.SetPause,
            self.AddCollection,
            self.RemoveCollection,
            self.UpdateRoyaltiesAddress,
            self.UpdateTreasuryAddress,
            self.UpdateFee,
            self.UpdateOracleAddress,
            self.UpdateRoyalties,
            self.UpdateAllowedTokens,
            self.SetOracleTolerance,
            self.UpdateMultisig,
        ]
        suites_list = []
        for test_class in test_classes_to_run:
            suites_list.append(
                unittest.TestLoader().loadTestsFromTestCase(test_class))

        big_suite = unittest.TestSuite(suites_list)
        unittest.TextTestRunner().run(big_suite)


if __name__ == "__main__":
    unittest.main()
