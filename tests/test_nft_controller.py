from test_env import Env, Keys, FA2Storage
import unittest
from pytezos.rpc.errors import MichelsonError
import logging
import json

logging.basicConfig(level=logging.INFO)

ALICE_PK = Keys().ALICE_PK
BOB_PK = Keys().BOB_PK
RESERVE_PK = Keys().RESERVE_PK

alice_pytezos = Keys().alice_pytezos
bob_pytezos = Keys().bob_pytezos
reserve_pytezos = Keys().reserve_pytezos

send_conf = dict(min_confirmations=1)


class TestNftController(unittest.TestCase):
    @ staticmethod
    def print_title(instance):
        print("Test NFT: " + instance.__class__.__name__ + "...")
        print("-----------------------------------")

    @ staticmethod
    def print_success(function_name):
        print(function_name + "... ok")
        print("-----------------------------------")

    class SetMetadata(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_sets_metadata_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            new_metadata = json.dumps({
                "name": "DOGA DOGA",
                "version": "1",
                "homepage": "https://dogami_forever.com/",
                "authors": ["DOGAMI <hello@hello.com>"],
                "interfaces": ["TZIP-012", "TZIP-021"]
            }).encode()
            set_metadata_param = {
                "set_id": "set1",
                "set_metadata_param": new_metadata.hex(),
            }
            nft_controller.setMetadata(set_metadata_param).send(**send_conf)

            self.assertEqual(nft.storage["metadata"]
                             ["content"](), new_metadata)

            TestNftController.print_success(
                "test01_it_sets_metadata_successfully")

        def test02_it_sets_metadata_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            new_metadata = json.dumps({
                "name": "DOGA DOGA",
                "version": "1",
                "homepage": "https://dogami_forever.com/",
                "authors": ["DOGAMI <hello@hello.com>"],
                "interfaces": ["TZIP-012", "TZIP-021"]
            }).encode()
            set_metadata_param = {
                "set_id": "set1",
                "set_metadata_param": new_metadata.hex(),
            }
            metadata = nft.storage["metadata"]()
            self.assertNotEqual(metadata, new_metadata)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setMetadata(set_metadata_param).send(**send_conf)
            self.assertEqual(
                nft.storage["metadata"](), metadata)
            bob_pytezos.contract(nft_controller.address).setMetadata(
                set_metadata_param).send(**send_conf)
            self.assertEqual(nft.storage["metadata"]
                             ["content"](), new_metadata)
            TestNftController.print_success(
                "test02_it_sets_metadata_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            new_metadata = json.dumps({
                "name": "DOGA DOGA",
                "version": "1",
                "homepage": "https://dogami_forever.com/",
                "authors": ["DOGAMI <hello@hello.com>"],
                "interfaces": ["TZIP-012", "TZIP-021"]
            }).encode()
            set_metadata_param = {
                "set_id": "set1",
                "set_metadata_param": new_metadata.hex(),
            }

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                nft_controller.setMetadata(
                    set_metadata_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            new_metadata = json.dumps({
                "name": "DOGA DOGA",
                "version": "1",
                "homepage": "https://dogami_forever.com/",
                "authors": ["DOGAMI <hello@hello.com>"],
                "interfaces": ["TZIP-012", "TZIP-021"]
            }).encode()
            set_metadata_param = {
                "set_id": "set1",
                "set_metadata_param": new_metadata.hex(),
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).setMetadata(
                    set_metadata_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            new_metadata = json.dumps({
                "name": "DOGA DOGA",
                "version": "1",
                "homepage": "https://dogami_forever.com/",
                "authors": ["DOGAMI <hello@hello.com>"],
                "interfaces": ["TZIP-012", "TZIP-021"]
            }).encode()
            set_metadata_param = {
                "set_id": "set1",
                "set_metadata_param": new_metadata.hex(),
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setMetadata(
                set_metadata_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.setMetadata(
                    set_metadata_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetBurnPause(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_sets_burn_pause_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            set_burn_pause_param = {
                "set_id": "set1",
                "set_burn_pause_param": True,
            }
            self.assertEqual(nft.storage["burn_paused"](), False)
            nft_controller.setBurnPause(set_burn_pause_param).send(**send_conf)

            self.assertEqual(
                nft.storage["burn_paused"](), True)

            TestNftController.print_success(
                "test01_it_sets_burn_pause_successfully")

        def test02_it_sets_burn_pause_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            set_burn_pause_param = {
                "set_id": "set1",
                "set_burn_pause_param": True,
            }

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setBurnPause(set_burn_pause_param).send(**send_conf)
            self.assertEqual(
                nft.storage["burn_paused"](), False)
            bob_pytezos.contract(nft_controller.address).setBurnPause(
                set_burn_pause_param).send(**send_conf)
            self.assertEqual(
                nft.storage["burn_paused"](), True)
            TestNftController.print_success(
                "test02_it_sets_burn_pause_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_burn_pause_param = {
                "set_id": "set1",
                "set_burn_pause_param": True,
            }

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                nft_controller.setBurnPause(
                    set_burn_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_nft_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            set_burn_pause_param = {
                "set_id": "set1",
                "set_burn_pause_param": True,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).setBurnPause(
                    set_burn_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            set_burn_pause_param = {
                "set_id": "set1",
                "set_burn_pause_param": True,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setBurnPause(
                set_burn_pause_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.setBurnPause(
                    set_burn_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class SetContractPause(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_sets_contract_pause_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            set_contract_pause_param = {
                "set_id": "set1",
                "set_contract_pause_param": True,
            }
            self.assertEqual(nft.storage["contract_paused"](), False)
            nft_controller.setContractPause(
                set_contract_pause_param).send(**send_conf)

            self.assertEqual(
                nft.storage["contract_paused"](), True)

            TestNftController.print_success(
                "test01_it_sets_contract_pause_successfully")

        def test02_it_sets_contract_pause_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            set_contract_pause_param = {
                "set_id": "set1",
                "set_contract_pause_param": True,
            }

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setContractPause(
                set_contract_pause_param).send(**send_conf)
            self.assertEqual(
                nft.storage["contract_paused"](), False)
            bob_pytezos.contract(nft_controller.address).setContractPause(
                set_contract_pause_param).send(**send_conf)
            self.assertEqual(
                nft.storage["contract_paused"](), True)
            TestNftController.print_success(
                "test02_it_sets_contract_pause_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            set_contract_pause_param = {
                "set_id": "set1",
                "set_contract_pause_param": True,
            }

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                nft_controller.setContractPause(
                    set_contract_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            set_contract_pause_param = {
                "set_id": "set1",
                "set_contract_pause_param": True,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).setContractPause(
                    set_contract_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            set_contract_pause_param = {
                "set_id": "set1",
                "set_contract_pause_param": True,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.setContractPause(
                set_contract_pause_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.setContractPause(
                    set_contract_pause_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class AddContract(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_adds_contract_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            self.assertNotIn(
                BOB_PK, nft.storage["contracts"]())

            nft_controller.addContract(
                add_contract_param).send(**send_conf)

            self.assertIn(
                BOB_PK, nft.storage["contracts"]())

            TestNftController.print_success(
                "test01_it_adds_contract_successfully")

        def test02_it_adds_contract_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.addContract(
                add_contract_param).send(**send_conf)
            self.assertNotIn(
                BOB_PK, nft.storage["contracts"]())
            bob_pytezos.contract(nft_controller.address).addContract(
                add_contract_param).send(**send_conf)
            self.assertIn(
                BOB_PK, nft.storage["contracts"]())
            TestNftController.print_success(
                "test02_it_adds_contract_with_two_admins")

        def test03_it_fails_when_factory_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                nft_controller.addContract(
                    add_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).addContract(
                    add_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.addContract(
                add_contract_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.addContract(
                    add_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class RemoveContract(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_removes_contract_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)

            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            nft_controller.addContract(add_contract_param).send(**send_conf)
            self.assertIn(BOB_PK, nft.storage["contracts"]())
            remove_contract_param = {
                "set_id": "set1",
                "remove_contract_param": BOB_PK,
            }
            nft_controller.removeContract(
                remove_contract_param).send(**send_conf)

            self.assertNotIn(BOB_PK, nft.storage["contracts"]())

            TestNftController.print_success(
                "test01_it_removes_contract_successfully")

        def test02_it_removes_contract_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            nft_addr = factory.storage["contract_sets"]["set1"]["nft"]()
            nft = alice_pytezos.contract(nft_addr)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            nft_controller.addContract(add_contract_param).send(**send_conf)
            self.assertIn(BOB_PK, nft.storage["contracts"]())

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            remove_contract_param = {
                "set_id": "set1",
                "remove_contract_param": BOB_PK,
            }
            nft_controller.removeContract(
                remove_contract_param).send(**send_conf)
            self.assertIn(BOB_PK, nft.storage["contracts"]())
            bob_pytezos.contract(nft_controller.address).removeContract(
                remove_contract_param).send(**send_conf)
            self.assertNotIn(BOB_PK, nft.storage["contracts"]())
            TestNftController.print_success(
                "test02_it_removes_contract_with_two_admins")

        def test03_it_fails_when_mint_controller_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)
            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            nft_controller.addContract(
                add_contract_param).send(**send_conf)

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            remove_contract_param = {
                "set_id": "set1",
                "remove_contract_param": BOB_PK,
            }

            with self.assertRaises(MichelsonError) as err:
                nft_controller.removeContract(
                    remove_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_mint_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            nft_controller.addContract(
                add_contract_param).send(**send_conf)
            remove_contract_param = {
                "set_id": "set1",
                "remove_contract_param": BOB_PK,
            }
            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).removeContract(
                    remove_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            factory.deploySet({"deploy": "set1"}).send(**send_conf)

            add_contract_param = {
                "set_id": "set1",
                "add_contract_param": BOB_PK,
            }
            nft_controller.addContract(
                add_contract_param).send(**send_conf)

            remove_contract_param = {
                "set_id": "set1",
                "remove_contract_param": BOB_PK,
            }
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            nft_controller.removeContract(
                remove_contract_param).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.removeContract(
                    remove_contract_param).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    class UpdateFactory(unittest.TestCase):
        def test00_before_tests(self):
            TestNftController.print_title(self)

        def test01_it_updates_factory_successfully(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            self.assertEqual(
                nft_controller.storage["factory"](), factory.address)
            nft_controller.updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                nft_controller.storage["factory"](), new_factory.address)

            TestNftController.print_success(
                "test01_it_updates_factory_successfully")

        def test02_it_updates_factory_with_two_admins(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            self.assertEqual(
                nft_controller.storage["factory"](), factory.address)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)
            nft_controller.updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                nft_controller.storage["factory"](), factory.address)
            bob_pytezos.contract(nft_controller.address).updateFactory(
                new_factory.address).send(**send_conf)
            self.assertEqual(
                nft_controller.storage["factory"](), new_factory.address)
            TestNftController.print_success(
                "test02_it_updates_factory_with_two_admins")

        def test03_it_fails_when_nft_controller_is_not_authorized_by_multisig(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            multisig.removeAuthorizedContract(
                nft_controller.address).send(**send_conf)

            with self.assertRaises(MichelsonError) as err:
                nft_controller.updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1009"})
            TestNftController.print_success(
                "test03_it_fails_when_nft_controller_is_not_authorized_by_multisig")

        def test04_it_fails_when_caller_is_not_an_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)

            with self.assertRaises(MichelsonError) as err:
                bob_pytezos.contract(nft_controller.address).updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1001"})
            TestNftController.print_success(
                "test04_it_fails_when_caller_is_not_an_admin")

        def test05_it_fails_when_called_twice_by_the_same_admin(self):
            factory, multisig, mint_controller, reveal_controller, nft_controller = Env(
            ).deploy_factory_app()
            new_factory = Env().deploy_factory(multisig=multisig.address)
            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            multisig.addAdmin(BOB_PK).send(**send_conf)
            multisig.setThreshold(2).send(**send_conf)

            nft_controller.updateFactory(
                new_factory.address).send(**send_conf)
            with self.assertRaises(MichelsonError) as err:
                nft_controller.updateFactory(
                    new_factory.address).send(**send_conf)
            self.assertEqual(err.exception.args[0]["with"], {
                             "int": "1004"})
            TestNftController.print_success(
                "test05_it_fails_when_called_twice_by_the_same_admin")

    def test_inner_test_class(self):
        test_classes_to_run = [
            self.SetMetadata,
            self.SetBurnPause,
            self.SetContractPause,
            self.AddContract,
            self.RemoveContract,
            self.UpdateFactory,
        ]
        suites_list = []
        for test_class in test_classes_to_run:
            suites_list.append(
                unittest.TestLoader().loadTestsFromTestCase(test_class))

        big_suite = unittest.TestSuite(suites_list)
        unittest.TextTestRunner().run(big_suite)

if __name__ == "__main__":
    unittest.main()
